This repository contains various implementations of the Dirichlet Process Mixture model. Currently, it contains
code for estimating a Transition density of a Markov process. If you use it please cite "Smooth Priors and the
Curse of Dimensionality: Feasibly Multivariate Density Estimation" by Minsu Chang and Paul Sangrey. It builds a
Python package that can be downloaded using conda from https://anaconda.org/sangrey/cdpm.
