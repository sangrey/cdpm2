"""Provide the plotting functions to use in the cdpm package."""
import matplotlib as mpl
import numpy as np
import seaborn as sns
import pandas as pd
import statsmodels.api as sm
from matplotlib import ticker
from scipy import stats


def pit_plot(data, ax=None, pct=.95, **kwargs):
    """
    Plot the data, which are assumed to be realizations of a probability integral transform (PIT).

    It also plots the pointwise confidence intervals under the null that the PIT variates are U[0,1].

    Parameters
    ----------
    data : pandas dataframe
    ax : matplotlib axes
    pct : float in [0,1]
    kwargs : parameters to be passed to the seaborn distplot function.

    """
    if ax is None:
        _, ax = mpl.pyplot.subplots()

    opt_num_bins = int(np.round(np.std(data)**(-1) * data.size**(1 / 3) / (2 * 3**(1 / 3) * np.pi**(1 / 6))))
    bins = np.linspace(0, 1, opt_num_bins)
    pit_ci_bottom, pit_ci_top = stats.binom.interval(alpha=pct, n=data.size, p=opt_num_bins**(-1))

    sns.distplot(data, norm_hist=True, kde=False, ax=ax, bins=bins, **kwargs)

    ax.axhline(pit_ci_bottom * (opt_num_bins / data.size), color='red', linestyle='dashed')
    ax.axhline(pit_ci_top * (opt_num_bins / data.size), color='red', linestyle='dashed')
    ax.axhline(1, color='black')
    ax.set_ylim([0, 2])
    ax.set_xlim([0, 1])
    ax.yaxis.set_major_locator(ticker.FixedLocator([0, .5, 1, 1.5, 2]))


def pit_acf_plot(data, ax, nlags=50, pct=.95, **kwargs):
    """
    Plot the autocorrelation function of the data and the associated Barlett bands.

    Under the null there is no dependence in the data and so the Barlett bands' width is constant.

    Parameters
    ----------
    data : pandas dataframe
    ax : matplotlib axes
    pct : float in [0,1]
    nlags : positive int
        The number of lags to plot
    kwargs : parameters to be passed to the seaborn distplot function.

    """
    ax.plot(np.arange(1, nlags), sm.tsa.acf(x=data, unbiased=True, nlags=nlags - 1, fft=True)[1:])
    pit_ci_bottom, pit_ci_top = stats.norm.interval(alpha=pct, scale=data.size**-.5, loc=0)

    ax.fill_between(x=np.arange(1, nlags), y1=pit_ci_bottom, y2=pit_ci_top, **kwargs)

    ax.set_ylim([-1, 1])
    ax.set_xlim([0, nlags - 1])


def fan_plot(ax, data, percentiles, cm=None, alpha=None, labels=None, **kwargs):
    """
    Create a fan plot using matplotlib and pandas.

    Parameters
    ----------
    ax : Axes
        The axes to draw to
    data : dataframe
        The data to plot.
    percentiles : tuple of positive ints
        The edges of the confidence bands.
    alpha : float, optional
        The alpha 'intensity' parameters.
    labels : tuple of strings, optional
        The labels of the artists.
    kwargs :
        Values to pass to the plot function.

    """
    from matplotlib import cm as mpl_cm
    cm = cm if cm is not None else mpl_cm.get_cmap()

    data_quantiles = pd.DataFrame(np.nanpercentile(data, percentiles, axis=1).T,
                                  index=data.index)
    num_sections = len(percentiles) // 2 + 1

    if len(percentiles) % 2 != 0:
        if labels is None:
            ax.plot(data_quantiles.iloc[:, num_sections - 1], **kwargs, zorder=num_sections+1)
        else:
            ax.plot(data_quantiles.iloc[:, num_sections - 1], label=labels[0], **kwargs, zorder=num_sections+1)

    if labels is None:
        for idx in range(num_sections):
            ax.fill_between(data_quantiles.index, data_quantiles.iloc[:, idx], data_quantiles.iloc[:, -(idx + 1)],
                            color=cm(idx / (num_sections - 1), alpha=alpha), zorder=idx)
    else:
        for label, idx in zip(reversed(labels[1:]), reversed(range(num_sections-1))):
            ax.fill_between(data_quantiles.index, data_quantiles.iloc[:, idx], data_quantiles.iloc[:, -(idx + 1)],
                            color=cm(idx / (num_sections - 1), alpha=alpha), label=label, zorder=idx)
