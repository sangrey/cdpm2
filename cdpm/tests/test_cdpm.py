"""Provide tests for the cdpm package."""
from hypothesis import given, strategies as st
import numpy as np
import pytest


np.random.seed(seed=424252)

@pytest.mark.filterwarnings("ignore::RuntimeWarning")
def test_importable():
    """Ensure that we can import the module, and all of its contents."""
    import cdpm


def test_simulation_importable():
    """Ensure that I can import the simulate_cdpm function."""
    from cdpm import simulate_cdpm


@given(st.floats(min_value=-1e10, max_value=1e10),
       st.floats(min_value=-1e10, max_value=1e10),
       st.floats(min_value=1e-5))
def test_normal_log_pdf(x, mean, scale):
    """Ensure that the logpdf is giving the same value as the scipy implementation."""
    from scipy.stats import norm
    from libcdpm import _normal_log_pdf as normal_log_pdf

    assert np.isclose(normal_log_pdf(x, mean, scale), norm.logpdf(x, mean, scale), equal_nan=True)


@given(st.integers(min_value=0, max_value=100))
def test_sort_indices(n_elem):
    """Test the extension function _sort_indices."""
    from libcdpm import _sort_indices as sort_indices
    from itertools import chain

    # If the range is already sorted, a flattened version should be the
    # original list.
    list_to_check = list(range(n_elem))
    assert list(chain.from_iterable(sort_indices(list_to_check))) == list_to_check, \
        "If the original list is a range, a flattened version should return the original list. "

    assert sort_indices([1, 2, 2, 1, 4]) == [[], [0, 3], [1, 2], [], [4]], \
        "Test a relatively standard case where we are missing some values. "

    assert sort_indices([0, 1, 2, 2, 3]) == [[0], [1], [2, 3, ], [4]], \
        "Test a relatively standard case."


@given(st.integers(min_value=2, max_value=40))
def test_conditional_gaussian_moments(dim):
    """Test that the extension computes conditional Guassian  moments correctly in a few special cases."""
    from libcdpm import _conditional_gaussian_moments
    first_idx = 0

    realization = np.ones(dim - 1)
    mean = np.zeros(dim)

    cov = np.eye(dim)

    cond_mean, cond_var = _conditional_gaussian_moments(first_idx, realization, mean, cov)

    assert np.isclose(cond_mean, mean[-1]) and np.isclose(cond_var, cov[-1, -1]), \
        "If we have independent realizations, conditiong should not change the value."

    cov = np.asarray([[1, .5], [.5, 1]])
    mean = np.zeros(2)
    true_cond_mean = .5
    true_cond_var = .75
    realization = 1

    cond_mean, cond_var = _conditional_gaussian_moments(first_idx, realization, mean, cov)

    assert np.isclose(cond_mean, true_cond_mean) and np.isclose(cond_var, true_cond_var), \
        "We calculated the true means and variance in a simple case."


@given(st.integers(min_value=1, max_value=30))
def test_is_stationary(n):
    """Test the the is_stationary function on some special cases."""
    from cdpm import is_stationary

    assert is_stationary(np.zeros((n, n))), "The zero matrix is stationary."

    assert not is_stationary(np.eye(n)), "The identity is not stationary."

    assert is_stationary(np.row_stack([np.random.standard_normal(n), .99 * np.eye(n)])), \
        "The first row should be ignored, and .99 * identity is stationary."

    assert not is_stationary(np.row_stack([np.random.standard_normal(n), np.eye(n)])), \
        "The first row should be ignored, and the identity is not stationary."
