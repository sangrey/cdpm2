"""Provide functions to simulate various datasets to use with the cdpm package."""
import numpy as np
from scipy import stats
from scipy import linalg as scilin
import cdpm
import pandas as pd
from functools import partial


def make_stationary(matrix):
    """
    Compute the nearest stationary matrix by forcing the eigenvalues to be less than 1 in modulus.

    It takes the eigendecomposition, and then divides the eigenvalues by 1.01 * modulus if they are greater than 1
    in modulus.

    Parameters
    ----------
    matrix : square ndarray of floats
        The matrix to make stationary.

    Returns
    -------
    stationary_mat : square ndarray of floats
        The closest stationary matrix to the given argument.

    """
    eigvals, eigvecs = scilin.eig(np.atleast_2d(matrix))
    eigvals[(np.abs(eigvals) >= 1)] /= 1.01 * np.abs(eigvals[(np.abs(eigvals) >= 1)])
    stationary_mat = np.real(eigvecs.dot((eigvals * scilin.pinv(eigvecs))))

    return stationary_mat


def simulate_cdpm(time_dim, beta_mu=0, beta_sigma=1, stick_scale=1, precn_df=5, precn_scale=1,
                  include_mean=False):
    """
    Simulate the cdpm model.

    Parameters
    ----------
    time_dim : positive int
        The number of periods
    beta_me : float
        The prior mean of the AR coefficients.
    beta_sigma : positive float
        The prior precision for the AR coefficients.
    stick_scale : positive float
        The beta parameter for the beta variable in the stick-breaking coefficient.
    precn_df : positive int
        The prior degrees of freedom for the innovation precision.
    precn_scale : positive float
        The prior scale for the innovation precision.
    include_mean : optional, bool

    """
    beta_mu = np.ravel(beta_mu)

    # Draw beta and sigma from their prior.
    def beta_prior():

        draw = stats.multivariate_normal(beta_mu.ravel(), beta_sigma).rvs()

        # We force stationarily.
        if draw.size > 1:
            draw[1:] = make_stationary(draw[1:])
        else:
            draw = make_stationary(draw)

        return draw

    precn_prior = stats.wishart(df=precn_df, scale=precn_scale)
    precn_prior = stats.wishart(df=precn_df, scale=precn_scale)

    alpha_series = []
    cluster_series = []  # history of clusters
    beta_mat = []
    precn_mat = []
    sticks = []

    # initial period
    alpha_series.append(0.0)  # initial alpha value is 0.0
    beta_mat.append(beta_prior())  # initial beta and sigma drawn from the prior
    precn_mat.append(precn_prior.rvs())
    cluster_series.append(0)
    sticks.append(np.random.beta(1, stick_scale))
    probabilities = cdpm.compute_probabilities(sticks)

    # transition probability used in stick-breaking representation

    cl_idx = 0
    for time_idx in range(time_dim):

        if include_mean:
            alpha = (beta_mat[cl_idx, 0] + np.dot(beta_mat[cl_idx, 1:], alpha_series[time_idx]) +
                     precn_mat[cl_idx]**(-.5) * np.random.standard_normal())
        else:
            alpha = (np.dot(beta_mat[cl_idx], alpha_series[time_idx]) +
                     precn_mat[cl_idx]**(-.5) * np.random.standard_normal())

        alpha_series.append(np.asscalar(alpha))

    # Determine which cluster alpha lies using the uniform random variable. If it's from existing cluster,
    # then just pick existing regime/beta/sigma (when random value is smaller than one of the threshold values).
    # If it's greater than the threshold, it means that new regime emerges. Draw beta/sigma from prior.

        omega_unif = np.random.uniform(0, 1)

        # I draw I new cluster if the uniform random variable is too big.
        while (omega_unif > np.sum(probabilities)):
            sticks.append(np.random.beta(1, stick_scale))
            beta_mat.append(beta_prior())
            precn_mat.append(precn_prior.rvs())
            probabilities = cdpm.compute_probabilities(sticks)

        cl_idx = np.argmin(omega_unif > np.cumsum(probabilities))

        cluster_series.append(cl_idx)

    # We drop the initial observation
    cluster_series = cluster_series[1:]
    alpha_series = alpha_series[1:]

    return (np.asarray(cluster_series), np.asarray(alpha_series), np.asarray(beta_mat), np.asarray(precn_mat),
            np.asarray(sticks))


def simulate_star(phi1, phi2, sigma, scalep, locp, df=None, time_dim=100, start_date='1995', freq='W'):
    """
    Simulate the stationary autoregressive model.

    Parameters
    --------
    phi1 : scalar
        The first autoregressive scaling term.
    phi2 : scalar
        The second autoregresive scaling term.
    scalep : scalar
        The scaling paramter for the lagged process inside the exponential function.
    locep : scalar
        The location paramter for the lagged process inside the exponential function.
    df : positive int, optional
        The degrees of freedom of the t-distributed innovations. If it is None,
        use Gaussian errors.
    start_date : datelike, optional
        When to start the simulation.
    freq : frequency string
        The frequency to date the simulation at.
    Returns
    ------
    return_data : dataframe

    """
    if df is None:
        variates = stats.norm.rvs
    else:
        variates = partial(stats.t.rvs, df=df)

    alpha_draws = []
    alpha_draws.append(variates(loc=0, scale=sigma))  # intial value

    for t in range(1, time_dim):

        transition_term = (1 + np.exp(-scalep * (alpha_draws[t - 1] - locp)))**(-1)
        alpha_draws.append(phi1 * alpha_draws[t - 1] * (1 - transition_term)
                           + phi2 * alpha_draws[t - 1] * transition_term + variates(loc=0, scale=sigma))

    return_data = pd.DataFrame(np.column_stack([np.asanyarray(alpha_draws)]),
                               columns=['alpha'], index=pd.date_range(start=start_date, freq=freq, periods=time_dim))

    return return_data


def compute_covariance_for_multiv_regime_switching(dim, gamma, sigma=1):
    """
    Compute the covariance for the regime switching model.

    Paramters
    --------
    dim : positive int
    gamma : scalar
    sigma : positive scalar, optional

    Returns
    -------
    ndarray

    """
    cov = np.eye(dim)

    for i in range(dim):
        for j in range(i + 1, dim):
            if gamma == 0:
                cov[j, i] = cov[i, j] = 0**(j - i)
            else:
                cov[j, i] = cov[i, j] = (.7)**(j - i)

    return cov * sigma**2


def simulate_multiv_regime_switching(beta, mu, sigma, prop_mean, prop_coeff, time_dim=500,
                                     start_date='1995', freq='W'):
    """
    Simulate a multivariate regime switching model.

    Paramters
    --------
    beta : ndarray
        The data's autoregressive coefficient
    mu : ndarray
        The correlation location
    prop_mean : ndarray
        The average probability of switching.
    prop_coeff : ndarray
        The deviation loading
    time:dim : postiive int, optional
        The number of time periods.
    start_date : datelike, optional
        The starting date for the simulated data
    freq : frequency string

    """
    state_dim = beta.shape[1]

    gamma_draws = np.empty(time_dim, dtype='int')
    alpha_draws = np.empty((time_dim, state_dim))
    # start with low correlation regime (normal times)

    gamma_draws[0] = 0
    covs = compute_covariance_for_multiv_regime_switching(state_dim, gamma_draws[0], sigma)
    alpha_draws[0] = stats.multivariate_normal.rvs(cov=covs)

    for t in range(1, time_dim):
        sample_covs = (np.atleast_2d(alpha_draws[t - 1]).T  @ np.atleast_2d(alpha_draws[t - 1]))
        temp_corr = (np.sum(sample_covs) - np.sum(np.diag(sample_covs))) / (sigma**2 * state_dim
                                                                            * np.maximum(1, state_dim - 1))
        prop = stats.norm.cdf(prop_mean[gamma_draws[t - 1]] + prop_coeff *
                              abs(temp_corr - mu[1 - gamma_draws[t - 1]])**2)

        if prop < stats.uniform.rvs(0, 1):
            gamma_draws[t] = abs(1 - gamma_draws[t - 1])
        else:
            gamma_draws[t] = gamma_draws[t - 1]

        covs = compute_covariance_for_multiv_regime_switching(state_dim, gamma_draws[t], sigma)
        alpha_draws[t] = (alpha_draws[t - 1] @ beta + stats.multivariate_normal.rvs(cov=covs))

    return_data = pd.DataFrame(np.column_stack([alpha_draws, gamma_draws]),
                               index=pd.date_range(start=start_date, freq=freq, periods=time_dim))
    return_data.columns = ['Asset {}'.format(n) for n in range(return_data.shape[1] - 1)] + ['Regime']

    return return_data
