"""Provide miscellaneous functions to use in the cdpm package."""
import pandas as pd
import numpy as np
from scipy.linalg import eig
from itertools import islice
import cdpm
import tables
from multiprocessing import get_context
from itertools import repeat
from copy import deepcopy


def get_eigvecs_with_eigval_close_to_1(mat):
    """
    Compute the eigenvectors associated with an eigenvalue close to 1.

    Parameters
    ---------
    mat : ndarray
       A transition matrix.
    Returns
    -------
    pandas dataframe

    """
    mat = mat[np.isfinite(mat)]
    mat_size = mat.size

    try:
        mat = mat.reshape((int(mat_size**.5), int(mat_size**.5))).T
    except ValueError:
        raise ValueError('The matrix must be square. The size of the attempted matrix is {}'.format(mat_size))

    # I have to make the matrix a valid transition matrix.
    rescaling_factor = np.sum(mat, axis=0)
    nonzero_entries = (rescaling_factor > 0)

    eigvals, eigvecs = eig(np.atleast_2d(mat[nonzero_entries, nonzero_entries].T).T /
                           rescaling_factor[nonzero_entries])
    idx = np.argmax(np.real(eigvals))
    return_eigvecs = np.real(eigvecs[:, idx])

    # I must undo the rescaling above.
    stationary_dist = (return_eigvecs / np.sum(return_eigvecs) * rescaling_factor[nonzero_entries])
    stationary_dist.resize(mat.shape[0])
    return_df = pd.DataFrame(stationary_dist)

    return return_df.T


def compute_stationary_distribution(trans_mat_draws, progress_bar=None):
    """
    Compute the stationary distributions associated with the transition matrices in its argument.

    Parameters
    ---------
    trans_mat_draws : 3d ndarray
        A collection of transition matrices.
    Returns
    -------
        pandas dataframe

    """
    if progress_bar is None:
        stat_dist = map(get_eigvecs_with_eigval_close_to_1, trans_mat_draws)
    else:
        stat_dist = map(get_eigvecs_with_eigval_close_to_1, progress_bar(trans_mat_draws))

    return_df = pd.concat(stat_dist, join='outer')

    return return_df.replace(np.nan, 0)


def predict_draws(cluster_identity_draws, trans_mat_draws, beta_draws,
                  comp_cov_draws, regressor, count=None, progress_bar=None):
    """
    Take the draws as given and compute the one stead ahead forecast from them.

    Parameters
    ---------
    cluster_identity_draws : 2d ndarray
        A matrix containing the cluster identies in each time period for each draw.
    trans_mat_draws : 3d ndarray
        An array containing the transition matrices for each draw.
    beta_draws : 4d ndarray
        An array containing the regression coeficients in each cluster for each draw.
    comp_cov_draws: 4d ndarray
        An array containing the regression covariances in each cluster for each draw.
    regressor : 2d ndarray
        A matrix containing the regressor.
    count : int
        The number of draws to return.
    Returns
    ------
    data_est : 3d ndarray
        The draws in each period for each draw.

    """
    num_draws = cluster_identity_draws.shape[0]

    if count is not None:
        step = min(num_draws, num_draws // count)
    else:
        step = 1

    it1 = zip(np.nan_to_num(cluster_identity_draws), np.nan_to_num(trans_mat_draws), beta_draws,
              comp_cov_draws)

    if progress_bar is None:
        it2 = it1
    else:
        it2 = progress_bar(it1, total=num_draws // step)

    forecasts = np.stack([cdpm.predict_cdpm(*args, regressor=regressor)
                          for args in islice(it2, 0, num_draws, step)], axis=-1)
    freq_offset = pd.tseries.frequencies.to_offset(pd.infer_freq(regressor.index))
    _index = pd.Series(regressor.index + freq_offset, name='date')

    it3 = zip(np.swapaxes(forecasts, 0, 1), regressor.columns[1:])
    fcst_list = [pd.DataFrame(arr, index=_index).assign(variable=col)
                 for arr, col in it3]

    return pd.concat(fcst_list, axis=0).set_index('variable', append=True)


def estimate_cdpm(regressor, regressand, num_draws, prior, progress_bar=None,
                  filename='../results/estimates.tmp.hdf', clusters=False,
                  data=None):
    """
    Estimates the model using the Conditional Dirichet Process Mixture Sampler.

    Parameters
    --------
    regressor : dataframe
    regressand : dataframe
    prior : dict of dicts...
    num_draws : int
    progress_bar : tqdm-like option, optional
    filename : str, optional
    clusters : array of int or bool, optional
        If it is False, they are initialized to equal 0. If it equals "random", it chooses them randomly from
        {1,2}. Otherwise, it assumes they should be fed into the function.

    Returns
    ------
    return_dict : dict
        the estimates.

    """
    if progress_bar is None:
        iterator = range((num_draws))
    else:
        iterator = progress_bar(range((num_draws)))

    regressor_dim = regressor.shape[1]
    regressand_dim = regressand.shape[1]
    time_dim = regressor.shape[0]

    if isinstance(clusters, str) and clusters == 'random':
        model = cdpm.CondDirichletProcessMix(regressor=regressor, regressand=regressand, prior=prior,
                                             cluster_id=np.random.choice(2, size=time_dim))
    elif clusters is False:
        beta_mu = np.array(prior['coeff']['mean']).reshape((regressor_dim, regressand_dim))
        comp_cov = (prior['cov']['scale'] / prior['cov']['df']) * np.eye(regressand_dim)
        model = cdpm.CondDirichletProcessMix(beta=beta_mu, comp_cov=comp_cov, regressor=regressor,
                                             regressand=regressand)
    elif np.isscalar(clusters):
        model = cdpm.CondDirichletProcessMix(regressor=regressor, regressand=regressand, prior=prior,
                                             cluster_id=np.random.choice(np.int(clusters), size=time_dim))
    else:
        model = cdpm.CondDirichletProcessMix(regressor=regressor, regressand=regressand,
                                             cluster_id=np.ravel(clusters), prior=prior)

    float_atom = tables.Atom.from_dtype(np.dtype(np.float64))
    int_atom = tables.Atom.from_dtype(np.dtype(np.int64))
    coeff_atom = tables.Atom.from_kind('float', shape=(regressor_dim, regressand_dim))
    cov_atom = tables.Atom.from_kind('float', shape=(regressand_dim, regressand_dim))

    with tables.File(filename, 'w') as file:
        file.create_vlarray(where='/', name='beta', atom=coeff_atom, expectedrows=num_draws)
        file.create_vlarray(where='/', name='sticks', atom=float_atom, expectedrows=num_draws)
        file.create_vlarray(where='/', name='comp_cov', atom=cov_atom, expectedrows=num_draws)
        file.create_vlarray(where='/', name='cov_mean', atom=cov_atom, expectedrows=num_draws)
        file.create_vlarray(where='/', name='trans_mat', atom=float_atom, expectedrows=num_draws)
        file.create_earray(where='/', name='beta_mean', atom=float_atom, expectedrows=num_draws,
                           shape=(0, regressor_dim, regressand_dim))
        file.create_earray(where='/', name='cluster_identity', atom=int_atom,
                           shape=(0, regressor.shape[0]), expectedrows=num_draws)
        regressand_index = np.array(regressand.index.strftime('%Y-%m-%d')).astype(np.string_)
        regressor_index = np.array(regressor.index.strftime('%Y-%m-%d')).astype(np.string_)

        file.create_earray(where='/', name='regressor_index', obj=regressor_index)
        file.create_earray(where='/', name='regressor', obj=regressor.values)
        file.create_earray(where='/', name='regressor_columns',
                           obj=np.array(regressor.columns).astype(np.string_))
        file.create_earray(where='/', name='regressand', obj=regressand.values)
        file.create_earray(where='/', name='regressand_index', obj=regressand_index)
        file.create_earray(where='/', name='regressand_columns',
                           obj=np.array(regressand.columns).astype(np.string_))
        if data is not None:
            data_index = np.array(data.index.strftime('%Y-%m-%d')).astype(np.string_)

            file.create_earray(where='/', name='data', obj=data.values)
            file.create_earray(where='/', name='data_index', obj=data_index)
            file.create_earray(where='/', name='data_columns',
                               obj=np.array(data.columns).astype(np.string_))

        for draw_idx in iterator:
            cluster, sticks, beta, beta_mean, cov, cov_mean, trans_mat = model()

            file.root.cluster_identity.append([np.ravel(cluster)])
            file.root.sticks.append(sticks.ravel())
            file.root.beta.append([coeff for coeff in beta])
            file.root.trans_mat.append([mat for mat in np.ravel(trans_mat)])
            file.root.comp_cov.append([mat for mat in cov])
            file.root.cov_mean.append([mat for mat in cov_mean])
            file.root.beta_mean.append([beta_mean])

    return read_results(filename)


def drop_nan_clusters_square(ndarrays):
    """
    Drop the all nan columns and rows from square ndarrays where both dimensions are padded.

    Parameters
    ---------
    iterable of ndarrays

    Returns
    ------
    list of ndarrays

    """
    returnval = [arr[np.isfinite(arr)].reshape((int(np.sum(np.isfinite(arr))**.5),
                                                int(np.sum(np.isfinite(arr))**.5)))
                 for arr in ndarrays]

    return returnval


def drop_nan_clusters(ndarrays):
    """
    Drop the all nan rows.

    Parameters
    ---------
    iterable of ndarrays

    Returns
    ------
    list of ndarrays

    """
    returnval = [arr[np.isfinite(arr)].reshape((-1,) + arr.shape[1:])
                 for arr in ndarrays]

    return returnval


def make_same_shape(ndarrays, num_changed_dim=1):
    """
    Stack the arrays in ndarrays padding with np.nan as necessary.

    If the ndarrays do not have the dtype np.float, return them unchanged.

    Parameters
    ---------
    iterable of ndarrays
    num_changed_dims : int, optional

    Returns
    -------
    ndarray

    """
    ndarrays_in = [np.asanyarray(arr) for arr in ndarrays]
    if ndarrays_in[0].dtype == np.float:
        max_cluster_dim = np.max([arr.shape[0] for arr in ndarrays_in])
        non_changed_dims = [(0, 0) for _ in range(ndarrays_in[0].ndim - num_changed_dim)]

        returnval = np.stack([np.pad(mat, [(0, max_cluster_dim - mat.shape[0])] * num_changed_dim
                                     + non_changed_dims, mode='constant',
                                     constant_values=np.nan) for mat in
                              deepcopy(ndarrays_in)])
    else:
        returnval = deepcopy(ndarrays)

    return returnval


def read_results(filename='../results/estimates.tmp.hdf'):
    """
    Read the results in the pytables store given in the filename.

    Parameters
    ---------
    filename : str,

    Returns
    ------
    return_dict : dict
        the estimates.

    """
    with tables.File(filename, 'r') as file:
        return_dict = {arr.name: make_same_shape(arr.read()) for arr in
                       file.list_nodes('/') if arr.name != 'trans_mat'}

        try:
            trans_mat = file.get_node('/trans_mat').read()
            return_dict['trans_mat'] = np.stack(make_same_shape([arr.reshape(int(arr.size**.5),
                                                                             int(arr.size**.5)) for arr
                                                                 in trans_mat], 2))
        except tables.NoSuchNodeError:
            pass

        regressand_index = pd.to_datetime(return_dict.pop('regressand_index').astype(str))
        regressor_index = pd.to_datetime(return_dict.pop('regressor_index').astype(str))

        if 'cluster_identity' in return_dict:
            return_dict['cluster_identity'] = pd.DataFrame(return_dict['cluster_identity'].T,
                                                           index=regressand_index).T

        return_dict['regressor'] = pd.DataFrame(return_dict['regressor'], index=regressor_index,
                                                columns=return_dict.pop('regressor_columns').astype(str))
        return_dict['regressand'] = pd.DataFrame(return_dict['regressand'], index=regressand_index,
                                                 columns=return_dict.pop('regressand_columns').astype(str))

        if 'data' in return_dict:
            data_index = pd.to_datetime(return_dict.pop('data_index').astype(str))
            return_dict['data'] = pd.DataFrame(return_dict['data'], index=data_index,
                                               columns=return_dict.pop('data_columns').astype(str))

    return return_dict


def estimate_var(beta_mu, comp_cov, regressor, regressand, num_draws, prior, progress_bar=None,
                 filename='../results/estimates.tmp.hdf', data=None):
    """
    Estimates the model using the Conditional Dirichet Process Mixture Sampler with at most one component.

    Parameters
    --------
    regressor : dataframe
    regressand : dataframe
    prior : dict of dicts...
    num_draws : int
    progress_bar : tqdm-like option, optional
    max_cluster_dim : int, optional
    filename : str, optional

    Returns
    ------
    return_dict : dict
        the estimates.

    """
    if progress_bar is None:
        iterator = range((num_draws))
    else:
        iterator = progress_bar(range((num_draws)))

    regressor_dim = regressor.shape[1]
    regressand_dim = regressand.shape[1]

    float_atom = tables.Atom.from_dtype(np.dtype(np.float64))

    with tables.File(filename, 'w') as file:
        file.create_earray(where='/', name='beta', atom=float_atom, expectedrows=num_draws,
                           shape=(0, regressor_dim, regressand_dim))
        file.create_earray(where='/', name='comp_cov', atom=float_atom, expectedrows=num_draws,
                           shape=(0, regressand_dim, regressand_dim))
        regressand_index = np.array(regressand.index.strftime('%Y-%m-%d')).astype(np.string_)
        regressor_index = np.array(regressor.index.strftime('%Y-%m-%d')).astype(np.string_)

        file.create_earray(where='/', name='regressor_index', obj=regressor_index)
        file.create_earray(where='/', name='regressor', obj=regressor.values)
        file.create_earray(where='/', name='regressor_columns',
                           obj=np.array(regressor.columns).astype(np.string_))
        file.create_earray(where='/', name='regressand', obj=regressand.values)
        file.create_earray(where='/', name='regressand_index', obj=regressand_index)
        file.create_earray(where='/', name='regressand_columns',
                           obj=np.array(regressand.columns).astype(np.string_))

        if data is not None:
            data_index = np.array(data.index.strftime('%Y-%m-%d')).astype(np.string_)

            file.create_earray(where='/', name='data', obj=data.values)
            file.create_earray(where='/', name='data_index', obj=data_index)
            file.create_earray(where='/', name='data_columns', obj=np.array(data.columns).astype(np.string_))

        for draw_idx in iterator:

            beta, cov = cdpm.draw_OLS_params(regressor, regressand, prior)

            file.root.beta.append([beta])
            file.root.comp_cov.append([cov])

    return read_results(filename)


def _predict_ols_in(x):
    """Unwrap the _predict_ols function appropriately."""
    return cdpm._predict_ols(x[0].values, x[1], x[2])


def predict_ols(regressor, beta_draws, comp_cov_draws, progress_bar=None):
    """
    Run an OLS prediction in parrallel across draws.

    Paramters
    ---------
    regressor : ndarray
    beta_draws : ndarray
    comp_cov_draws : ndarray
    progress_bar : tqdm-like, optional

    Returns
    -----
    ndarray

    """
    iterator_in = zip(repeat(regressor), beta_draws, comp_cov_draws)
    iterator = (progress_bar(iterator_in, total=len(beta_draws))
                if progress_bar is not None else iterator_in)

    forecasts = np.stack(list(map(_predict_ols_in, iterator)), axis=0)

    freq_offset = pd.tseries.frequencies.to_offset(pd.infer_freq(regressor.index))
    _index = pd.Series(regressor.index + freq_offset, name='date')

    it = zip(np.swapaxes(forecasts, 0, 2), regressor.columns[1:])
    fcst_list = [pd.DataFrame(arr, index=_index).assign(variable=col)
                 for arr, col in it]

    return pd.concat(fcst_list, axis=0).set_index('variable', append=True)


def empirical_cdf(data, eval_points):
    """
    Compute the empirical cdf of the data.

    Parameters
    ----------
    data : array-like
    eval_points : array-like
    Returns
    -------
    return_vals : ndarray

    """
    data = np.sort(data).reshape(data.size)
    eval_points = np.ravel(eval_points)
    return_vals = np.asarray([np.mean(np.less_equal(data, point), axis=0) for point in eval_points])

    return return_vals


def probability_integral_transform(reference_data, evaluated_data, progress_bar=None):
    """
    Compute the probability integral transform of the data using the empirical cdf of the predicted data.

    The reference_data is assumed to be of the form (horizon, data) dimensions where evaluated_data is of
    length evaluated_data.

    Parameters
    ----------
    reference_data : 2d ndarray or DataFrame
    evaluated_data : 1d ndarray or DataFrame
    progress_bar : tqdm instance
        One of the tqdm iterator wrappers.

    Returns
    -------
    return_points : DataFrame

    """
    if progress_bar:
        iterator = zip(progress_bar(np.asanyarray(reference_data)), np.asanyarray(evaluated_data))
    else:
        iterator = zip(np.asanyarray(reference_data), np.asanyarray(evaluated_data))

    return_points = np.fromiter((empirical_cdf(data_day, prediction) for data_day, prediction in iterator),
                                dtype=np.float64, count=evaluated_data.shape[0])

    return_df = pd.DataFrame(return_points, columns=['PIT'])

    if hasattr(evaluated_data, "index"):
        return_df.index = evaluated_data.index

    return return_df


def compute_mean(row, trans_mat, coefficients):
    """
    Compute the mean given the transition matrix and coefficients.

    Parameters
    --------
    row : 1d ndarray
        The data
    trans_mat : 2d ndarray
    coefficients : 2d ndarray

    Returns
    ------
    1d ndarray

    """
    cluster_id = int(row[-1])
    coeff = coefficients[np.isfinite(coefficients)].reshape((-1,) +
                                                            coefficients.shape[1:])
    cluster_dim = coeff.shape[0]
    transition = trans_mat[np.isfinite(trans_mat)].reshape((cluster_dim,
                                                            cluster_dim))[cluster_id]
    val = transition @ (row[:-1] @ coeff)

    return np.squeeze(val)


def compute_mean_var(row, trans_mat, coefficients):
    """
    Compute the proportion of the variance arising from variation in the means.

    Paramters
    --------
    row : 1d ndarray
        The data
    trans_mat : 2d ndarray
    coefficients : 2d ndarray

    Returns
    ------
    1d ndarray

    """
    cluster_id = int(row[-1])
    coeff = coefficients[np.isfinite(coefficients)].reshape((-1,) + coefficients.shape[1:])
    cluster_dim = coeff.shape[0]
    probs = trans_mat[np.isfinite(trans_mat)].reshape((cluster_dim,
                                                       cluster_dim))[cluster_id]

    val = (probs @ (row[:-1] @ coeff)**2) - (probs @ (row[:-1] @ coeff))**2

    return np.squeeze(val)


def compute_innov_var(row, trans_mat, covariance):
    """
    Compute the proportion of the variance arising from variation in innovation variances.

    Paramters
    --------
    row : 1d ndarray
        The data
    trans_mat : 2d ndarray
    coefficients : 2d ndarray

    Returns
    ------
    1d ndarray

    """
    cov = covariance[np.isfinite(covariance)].reshape((-1,) + covariance.shape[1:])
    cluster_id = int(row[-1])
    cluster_dim = cov.shape[0]
    transition = trans_mat[np.isfinite(trans_mat)].reshape((cluster_dim,
                                                            cluster_dim))[cluster_id]
    val = cov.T @ transition

    # Only computes the variance
    return np.diag(val)


def compute_mean_and_var(regressor, regressand, trans_mat_draws,
                         cluster_id_draws, coeff_draws, comp_cov_draws,
                         progress_bar=None):
    """
    Compute the mean, variance from the mean, and variance from the innovations.

    Parameters
    ---------
    regressor : dataframe
    regressand : dataframe
    trans_mat_draws : 3d ndarray
    coeff_draws : 3d ndarray
    compov_cov_draws : 3d ndarray
    progress_bar : iterator wrapper, optional

    Returns
    ------
    mean : dataframe
    var_from_mean : dataframe
    innov_var : dataframe

    """
    iterator = list(zip(trans_mat_draws, cluster_id_draws.iterrows(),
                        coeff_draws, comp_cov_draws))

    if progress_bar is not None:
        it = progress_bar(iterator)
    else:
        it = iterator

    mean_draws = []
    mean_var_draws = []
    innov_var_draws = []

    for trans, (_, identity), coeff, cov in it:
        data_in = regressor.join(identity).dropna()
        mean_result = np.apply_along_axis(compute_mean, axis=1, arr=data_in.values,
                                          trans_mat=trans, coefficients=coeff)
        mean_var_result = np.apply_along_axis(compute_mean_var, axis=1, arr=data_in.values,
                                              trans_mat=trans, coefficients=coeff)
        var_result = np.apply_along_axis(compute_innov_var, axis=1,
                                         arr=data_in.values, trans_mat=trans,
                                         covariance=cov)

        innov_var_draws.append(var_result)
        mean_var_draws.append(mean_var_result)
        mean_draws.append(mean_result)

    mean = pd.DataFrame(np.mean(mean_draws, axis=0),
                        index=regressand.index[1:], columns=regressand.columns)
    var_from_mean = pd.DataFrame(np.mean(mean_var_draws, axis=0),
                                 index=regressand.index[1:],
                                 columns=regressand.columns)
    innov_var = pd.DataFrame(np.mean(innov_var_draws, axis=0),
                             index=regressand.index[1:],
                             columns=regressand.columns)

    return mean, var_from_mean, innov_var
