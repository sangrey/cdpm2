rng(1234)
T=50;
series_cluster = 1;
nums_exp = [];
nums_unif = rand(5,1);
lambda = 1/4;
nums_exp = cumsum(-log(1-nums_unif)/lambda)
N = 1;


for tt = 1:(T-1)

if nums_exp(N) <= tt && N <5
    N = N+1;
end
    
series_cluster = [series_cluster; randsample(N,1)];

end

plot(1:50, series_cluster, 'LineWidth', 2)