rng(1234)
T=50;
para_scale =0.3;

p = betarnd(1, para_scale);
check_break = p;
n = 1;
series_cluster = [];

for tt = 1:T
    
    val = rand(1);
    
    while val > check_break
        
        check_break = check_break + ((1-p)^n)*p;
        n = n+1;
        
    end

    series_cluster = [series_cluster; n];
    
    check_break = p;
    n = 1;
    
end

unique(series_cluster)

% relabeling

%for tt = 1:T   
%end
