\documentclass[11pt,handout, xcolor=svgnames, aspectratio=169]{beamer}
\usepackage{dirichlet_slides}
\addbibresource{cdpm.bib}

\title[Chang and Sangrey]{\texorpdfstring{What Lifts the Curse of Dimensionality?  \protect\\
\protect\large \vspace{.4\baselineskip} Feasible Multivariate Density Estimation}{What Lifts the Curse of
Dimensionality? Feasible Multivariate Density Estimation}} 

\author{\texorpdfstring{Minsu Chang and Paul Sangrey \newline \protect\vspace{.75\baselineskip} \itshape University
of Pennsylvania}{Minsu Chang and Paul Sangrey}}

\date{NBER-NSF SBIES Conference \protect\newline \protect\vspace{.25\baselineskip} May 25--26, 2018}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

\begin{frame}[plain]
    \maketitle
\end{frame}

\addtocounter{framenumber}{-1}


\begin{frame}{Problem: Multivariate Time Series Dynamics}

    \begin{itemize}
        \item Vector autoregressions (\textbf{VARs}) model conditional means.
            \pause
            \vspace{.75\baselineskip}
        \item Time series often exhibit \textbf{nonlinear}, \textbf{non-Gaussian} dynamics.
            \pause
            \vspace{.75\baselineskip}
                    \item We want to estimate the conditional density.
            \pause
            \vspace{.75\baselineskip}
        \item Various authors built models with time-varying parameters, regime switching, stochastic volatility,
            etcetera.
            \pause
            \vspace{.75\baselineskip}
        \item Two issues:
                \vspace{.5\baselineskip}
            \begin{enumerate}
                \item[1.] Practitioners cannot easily decide which model to use on their data.
                    \vspace{.5\baselineskip}
                \item[2.] Estimation is very difficult when there are  more then 3--4 series. (\bfseries \large
                    \textit{curse-of-dimensionality})
            \end{enumerate}
            \pause
    \end{itemize}

\end{frame}


\begin{frame}{This Paper}

    \begin{enumerate}
        \item Builds a {\bfseries nonparametric, easy-to-use} conditional density estimator that {\bfseries scales
            well} with the number of series.
            \vspace{.75\baselineskip}
       \item Provides precise conditions on when it approximates the data's dynamics well.
            \vspace{.75\baselineskip}
        \item Shows why our method doesn't have a {\bfseries \textit{curse of dimensionality.}}
                    \vspace{.75\baselineskip}
        \item Shows it works well with real macroeconomic data.
    \end{enumerate}

\end{frame}

\begin{frame}{Data Generating Process}

    \begin{itemize}

    \item We estimate $x_t$'s conditional densities for $t = 1, \ldots, T$: 
        \begin{equation*} 
            \truem\left(x_t \mvert \F_{t-1}\right) 
        \end{equation*}
\vspace{0.1in}
    \begin{enumerate}
        \item  $x_t$ is a vector-valued time series of dimension $D$.
        \vspace{1.0\baselineskip}
        \item $x_t$ is first-order hidden Markov.
        \vspace{1.0\baselineskip}
        \item $x_t$ is Gaussian process.
        \vspace{1.0\baselineskip}
        \begin{itemize}
            \item $\truem\left(x_t \mvert \F_{t-1}\right)$ has an  \textbf{infinite Gaussian mixture
                representation} for each $t$.
                \vspace{.5\baselineskip}
            \item $x_t$ has finite mean and finite variance.
        \end{itemize}
        \vspace{\baselineskip}
    \end{enumerate}
\end{itemize}
\end{frame}


\begin{frame}{Representation}

    \begin{itemize}
        \item These distributions are Gaussian mixtures
           \begin{equation*}
               \truem\left(x_{t} \mvert \F_{t-1}\right) = \sum_{k=1}^\infty \red{\pi_{k,t-1}} \cdot
               N(\red{\beta_{k,t}} x_{t-1}, \red{\Sigma_{k,t}})
           \end{equation*}
           for some $\red{\pi_{k,t-1}}$, $\red{\beta_{k,t}}$, \& $\red{\Sigma_{k,t}}$.
           \vspace{\baselineskip}
           \item We approximate $\truem$ with
               \begin{equation*}
                   \approxm\left(x_{t} \mvert \F_{t-1}\right) \coloneqq \sum_{k=1}^{\blue{K_T}}
                   \blue{\pi_{k,t-1}} \cdot N\left(\blue{\beta_{k}} x_{t-1}, \blue{\Sigma_{k}}\right)
               \end{equation*}       
               where \blue{$K_T$} is the number of clusters.
    \end{itemize}

\end{frame}



\begin{frame}
\frametitle{Contributions}
    \begin{enumerate}
        \item We construct an approximating process $\approxm$ using a mixture representation.
        \vspace{.5\baselineskip}
        \begin{itemize}
            \item Its number of components \blue{$K_T$} grows at a $\log(\red{T})$ rate.
            \item This rate is independent of the number of series \red{D}.
        \end{itemize} 
        \vspace{.5\baselineskip}
        \item We provide a computationally efficient Bayesian estimator $\estm$ for $\truem$'s marginal and
            transition densities.  \vspace{.5\baselineskip}
        \begin{itemize}
            \item Its marginal density converges at a rate $\sqrt{\log(\red{T})}/\sqrt{\red{T}}$.
            \item Its transition density converges at a rate $\log(\red{T})/\sqrt{\red{T}}$.
        \end{itemize} 
        \vspace{.5\baselineskip}
        \item  We show $\estm$ performs well in an empirical example.
    \end{enumerate}
\end{frame}

\begin{frame}{What metric --- $\delta(\estm , \truem)$ --- to use?}

    \begin{itemize}
        \item We care about conditional expectations and quantiles, not the density itself. 

            \vspace{.5\baselineskip}
            \pause


        \item[$\implies$]  We care about the distribution. 

            \vspace{.5\baselineskip}
            \pause

        \item \textbf{Hellinger} measures distance between distributions.            

        \begin{itemize}
            \item We get all the conditional forecasts, expectations and quantiles right. 
        \end{itemize}

            \vspace{.5\baselineskip}
            \pause

        \item The previous literature, mostly uses $\norm*{\estm-\truem}_{L^p}$  \parencite{stone1980optimal}.
            \vspace{.5\baselineskip}

            \begin{itemize}
                \item The $L^p$-norm heavily penalizes density deviations even when they only
                    weakly effect expectations. 
                    \vspace{.5\baselineskip}
                \item Only if $\truem$ is the parameter of interest, should  we use the $L^p$-norm.
                    \vspace{.5\baselineskip}
                \item The \textit{\bfseries curse of dimensionality} makes this infeasible in high dimensions.
            \end{itemize}
   \end{itemize}
        
\end{frame}


\begin{frame}
\frametitle{Literature Review}
    \begin{itemize}
        \item \textbf{Avoiding the \textit{Curse of Dimensionality}}: \\
            \textcite[AoS]{jiang2007bayesian},  \textcite[Bernoulli]{nguyen2016borrowing},
            \textcite[JASA]{guhaniyogi2015bayesian}, \textcite[WP]{koop2017bayesian}
            \vspace{.5\baselineskip}

        \item \textbf{Bayesian Conditional Density Estimation}: \\
            \textcite[NIPS]{lin2010construction}, \textcite[AoS]{norets2010approximation},
            \textcite[JoE]{norets2012bayesian}, \textcite[JMVA]{pati2013posterior},
            \textcite[JoE]{kalli2018bayesian} 
            \vspace{.5\baselineskip}

        \item \textbf{Nonlinear Parametric Models}: \\ 
            \textcite[Ecta]{hamilton1989new}, \textcite[ReStud]{kim1998stochastic},
            \textcite[WP]{smith2017detecting}
    \vspace{.5\baselineskip} 
    \end{itemize}
\end{frame}



\section{\texorpdfstring{Bounding the Approximation Error: \protect \\ \protect\vspace{.2\baselineskip}
        \protect\large Lifting the Curse of Dimensionality}{Bounding the Approximation Error }}


\begin{frame}{Main Idea}
    \begin{itemize}
        \item[1)] Concentration of Measure
            \vspace{.5\baselineskip}

        \begin{itemize}
            \item Bound the distribution's complexity.
        \end{itemize}
            \vspace{1.0\baselineskip}

        \item[2)] Construction of Sieve using Mixtures

        \begin{itemize}
                \vspace{.5\baselineskip}
            \item Develop a representation that achieves this bound.
                \vspace{.5\baselineskip}
            \item The number of components is independent of the number of series.
        \end{itemize}
    \end{itemize}
\end{frame}


\begin{frame}{Concentration of Measure}
    \begin{figure}[htb]
        \centering
        \includegraphics[width=.75\textwidth, height=.5\textheight]{figures/ball_volume.pdf} 
        \caption{Volume of a Ball Relative to a Hypercube}
    \end{figure}

        \vspace{-.2\baselineskip}

    \begin{itemize}
        \item Probability measures concentrate on balls. 
            \vspace{.5\baselineskip}
        \item We build an approximation for the ball, not the hypercube. 
    \end{itemize}

\end{frame}

\begin{frame}{Construction of Sieve using Mixtures}
  
    \begin{enumerate}
    \item $\widetilde{X}$ is a standardized Gaussian process.
            \vspace{.5\baselineskip}
        \item Define a random discretization operator --- $\blue{\Theta_T} :
            \mathbb{R}^{\red{T}\times \red{D}} \to
           \mathbb{R}^{\blue{K_T} \times \red{D}}$, \quad $\blue{K_T} \propto \log
            \left(\red{T}\right)$ 

            \vspace{.5\baselineskip}

        \begin{itemize}
            \item $\blue{\Theta_T}$ bins $\widetilde{X}$ thereby constructing a finite mixture sieve.
        \end{itemize}

        \pause
            \vspace{.5\baselineskip}

        \item $\blue{\Theta_T}$ does not change the first two moments of $\widetilde{X}$.
            \vspace{.5\baselineskip}

        \item The first moments are close. $\iff$ The densities are close.

            \vspace{.5\baselineskip}

        \item $\blue{\Theta_T'} \widetilde{X}'s$ densities form a sieve for $\widetilde{X}$'s densities.

    \end{enumerate}
    

\end{frame}



\begin{frame}{Representing the Transition Density}

    \begin{itemize}
        \item  $\blue{\gamma_t} \coloneqq (\theta_t, \theta_{t+1})$ form a discrete Markov chain.  
            \vspace{.5\baselineskip}

        \item $\blue{\gamma_t}$ takes on $ \propto \log(\red{T})^2$ values.  
            \vspace{.5\baselineskip}

        \item We can compute conditional distributions in closed-form.
            \vspace{.5\baselineskip}
            \begin{equation*}
                x_t \vert x_{t-1} , \blue{\gamma_{t-1}} \sim \sum_{\blue{\gamma_t=k}}
                  \blue{\pi}\left(\blue{\gamma_t} \mvert \blue{\gamma_{t-1}}\right) N\left(\blue{\beta_{k}}
                  x_{t-1}, \blue{\Sigma_{k}}\right) 
              \end{equation*}

          \item  The \blue{$\gamma_{t-1}$} are not identified.
            \vspace{.5\baselineskip}
            \begin{equation*}
              x_t \vert \F_{t-1} \sim \sum_{\blue{\gamma_{t-1}}} \sum_{\blue{\gamma_t=k}}
              \blue{\pi}\left(\blue{\gamma_t} \mvert \blue{\gamma_{t-1}}\right) N\left(\blue{\beta_{k}} x_{t-1},
              \blue{\Sigma_{k}}\right) \Pr_{\truem} \left(\blue{\gamma_{t-1}} \mvert \F_{t-1}\right) 
            \end{equation*}
            where  $\Pr_{\truem}$ refers to posterior distribution.
    \end{itemize}

\end{frame}

\begin{frame}[label=ContractionRates]{Contraction Rates}

    \begin{itemize}
        \item Let $\truem$ be a Uniform Hidden Markov Gaussian process with finite means $\mu_t$  and
            covariances $\red{\Sigma_t}$ and $\min_t \mathrm{eig} (\red{\Sigma_t}) > C_1 > 0$.
            \vspace{1\baselineskip}
            \item \green{The transition density's posterior} converges at $\log(\red{T}) / \sqrt{\red{T}}$.
            \vspace{1\baselineskip}
        \item The \green{marginal density's posterior} converges at $\sqrt{\log(\red{T})} / \sqrt{\red{T}}$.
            \vspace{1\baselineskip}
        \item \textbf{Our estimators don't have  a \textit{curse of dimensionality}!}
    \end{itemize}

    \vfill \hfill \hyperlink{ContractionRateThm}{\beamerbutton{Theorems}}

\end{frame}


\section{Empirics}


\begin{frame}[label=eststrategy]{Estimation Strategy}
               \begin{equation*}
                   \approxm\left(x_{t} \mvert \F_{t-1}\right) \coloneqq \sum_{k}
                   \blue{\pi_{k,t-1}} \cdot N\left(\blue{\beta_{k}} x_{t-1}, \blue{\Sigma_{k}}\right)
               \end{equation*}       

    \begin{itemize}
        \item We develop a Bayesian estimator using a Gibbs sampler.
        \vspace{.5\baselineskip}
    \item $\approxm$ is almost a standard Gaussian mixture model.
        \vspace{.5\baselineskip}
    \item \textbf{Complication:} The predictive distributions are time-varying. 
        \vspace{.5\baselineskip}
    \item[$\implies$] 
        We must construct a prior for \blue{$\pi\left(\gamma_t \mvert \gamma_{t-1}\right)$}. 
        \vspace{.5\baselineskip}
    \item By stacking the Dirichlet processes over time, we obtain a Dirichlet process over the
        $\blue{(\gamma_{t-1},\gamma_t)}$ product space.  
        \vspace{.5\baselineskip}
   % \item The stacked Dirichlet processes define a prior for \blue{$\pi\left(\gamma_t \mvert
%        \gamma_{t-1}\right)$}.
\hyperlink{algorithm}{\beamerbutton{Detail}} 
    \end{itemize}

\end{frame}




\begin{frame}{Empirical Example: CDPM}

    \begin{itemize}
        \item Data: Four monthly series from FRED. (Jan/1963 -- Jan/2017)
            \vspace{.5\baselineskip}
        \begin{itemize}
            \item Unemployment Rate
            \item Inflation 
            \item Housing Supply
            \item Real Personal Consumption Expenditure
        \end{itemize}
    \end{itemize}
    \begin{center}
        \includegraphics[width=.7\textwidth, height=.6\textheight]{figures/cdpm/data_series}
    \end{center}

\end{frame}

\begin{frame}
\frametitle{What can we learn from posterior draws?}
    \begin{tabularx}{\textwidth}{Y Y }
        Mean & Innovation Volatility \\
        \includegraphics[width=.49\textwidth, height=.25\textheight]{figures/cdpm/mean_forecast} & 
        \includegraphics[width=.49\textwidth, height=.25\textheight]{figures/cdpm/std_from_innov} \\
        Coefficient Volatility & Cluster Probabilities \\
        \includegraphics[width=.49\textwidth, height=.25\textheight]{figures/cdpm/std_from_mean} &
        \includegraphics[width=.49\textwidth, height=.25\textheight]{figures/cdpm/cluster_probs} \\
        \end{tabularx}

    \footnotesize

    \begin{itemize}
        \item Coefficient dynamics changes more / Smaller change in innovation volatility.
        \item Mean tracks the data very well.
        \item Number of clusters is higher in recessions. Complexity increases these times.
    \end{itemize}
\end{frame}


\begin{frame}{1-Period Conditional Forecasts: CDPM}
    \begin{tabularx}{\textwidth}{Y Y}
        Unemployment Rate & Housing Supply \\ 
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures/cdpm/UNRATE_forecasts} &
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures/cdpm/HSUPPLY_forecasts} \\ 
        Inflation & Consumption Expenditure Change \\
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures/cdpm/INFLA_forecasts} &
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures/cdpm/RCONSMP_forecasts} \\
    \end{tabularx}
\end{frame}



\begin{frame}{1-Period Conditional Forecasts: CDPM}
    \begin{tabularx}{\textwidth}{Y Y }
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures/cdpm/RCONSMP_pit} & 
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures/cdpm/RCONSMP_acf} \\ 
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures/cdpm/UNRATE_pit} & 
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures/cdpm/UNRATE_acf} \\ 
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures/cdpm/INFLA_pit} & 
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures/cdpm/INFLA_acf} \\
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures/cdpm/HSUPPLY_pit} & 
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures/cdpm/HSUPPLY_acf} \\ 
    \end{tabularx}
                  
\end{frame}



\begin{frame}{Conclusion}

    \begin{enumerate}
        \item We construct a nonparametric Bayesian density estimator $\estm$ for the transition density of a
            first-order hidden Markov Gaussian Process.
            \vspace{.25\baselineskip}
        \begin{itemize}
            \item Its marginal density converges at the rate $\sqrt{\log(\red{T})}/\sqrt{\red{T}}$.
                \vspace{.25\baselineskip}
            \item Its transition density converges at the rate $\log(\red{T})/\sqrt{\red{T}}.$
        \end{itemize} 
            \vspace{.5\baselineskip}
        \item These rates are remarkable as they are independent of number of series, i.e.\@ we do not suffer from
            a {\bfseries \large \textit{curse of dimensionality}.}
            \vspace{.5\baselineskip}
        \item We do this by exploiting the probability measures' behavior in high dimensions.
            \vspace{.5\baselineskip}
        \item  We show $\estm$ performs well in an empirical example.
    \end{enumerate}
    
\end{frame}


\section{Appendix}


\begin{frame}[label = thetamat, noframenumbering, plain]{Generalized Selection Matrix}

    \begin{definition}
        Let $\chi$ be a Bernoulli random variable with $\mu \coloneqq Pr(\chi = 1) \in (0,1)$. 
        Draw another random variable $\xi$ from  $\lbrace -1, 1 \rbrace$ with probability $1/2$ each.
        Let $T \in \mathbb{N}$ be given.\

        Draw variables $\theta \coloneqq \xi \cdot \chi$ independently, and form them into column vectors of
        length $T$, $\{\theta_k\}$  until all of the rows of $\Theta$ contain at least one nonzero element.
    \end{definition}

    \begin{itemize}
        \item Properties: $E(\theta) = 0$, $Var(\theta) = \abs*{\theta} = \mu$, rows are i.i.d., and columns are a
            martingale difference sequence.
            \vspace{\baselineskip}
        \item \blue{$K_T$} is the (endogenous) number of columns of $\Theta$. 
    \end{itemize}

    \vfill \hfill \hyperlink{genmat}{\beamerbutton{Return}}
\end{frame}

\begin{frame}[label = jointApproximationThoerem, noframenumbering, plain]{Norms Close $\implies$ Densities Close}

    \begin{theorem}
        \label{thm:joint_density}
        Let $\tilde{x}_1, \ldots \tilde{x}_T$ be a D-dimensional standardized Gaussian process with stochastic
        means $\mu_t$ and covariances $\Sigma_t$, where $\Sigma_t$ is positive-definite for all $t$.
        Let $\truem$ denote the density of $X$.
        Then given $\epsilon > 0$, for some $\eta \in (0,1)$ with probability at least $1-\eta$ with respect
        to the $\Theta$ space, the implied density --- $\approxm$ --- of $\Theta' X$ satisfies the
        following. 
    
        \begin{equation*}
            h^2\left(\truem(\widetilde{X}), \approxm(\widetilde{X})\right) < \epsilon
        \end{equation*}
    
    \end{theorem}

    \vfill \hfill \hyperlink{JointDensityApproximation}{\beamerbutton{Return}}

\end{frame}

\begin{frame}[label = CondDensityApproxThm, noframenumbering, plain]{Representing the Conditional Density}

    \begin{theorem}
        \label{thm:transition_density}
        Let $x_1 \ldots x_T \in R^{T \times D}$ be a Uniformly Geometric hidden Markov Gaussian process with
        density $\truem$.
        Let $\epsilon > 0$ be given.  Let $\blue{K_T^2} \geq c \log(T)^2 / \epsilon$ for some global constant $c$. 
        Then there exists a mixture density $\approxm$ with $K_T^2$ clusters, such that 
    
        \begin{equation*}
            \approxm\left(x_t \mvert x_{t-1}, \gamma_{t-1} \right) \coloneqq \sum_{k=1}^{\blue{K_T^2}}
            \phi\left(x_{t-1} \beta_k, \Sigma_k\right) \Pr\left(\gamma_{t} = k \mvert \gamma_{t-1}\right) 
        \end{equation*}
    
        and, up to a set with probability zero, where $\truem\left(x_t \mvert \F_{t-1}\right)$ is the true density
    
        \begin{equation*}
            \sup_{x_{t-1}} h^2\left(\approxm\left(x_t \mvert x_{t-1}\right), \truem\left(x_t \mvert
            \F_{t-1}\right)\right) < \epsilon
        \end{equation*}
    
    \end{theorem}

    \vfill \hfill \hyperlink{ConditionalDensityApproximation}{\beamerbutton{Return}}

\end{frame}

\begin{frame}[label=ContractionRateThm, noframenumbering, plain]{Transition Density Contraction Rates}

    \begin{theorem} Let $\truem$ be a Hidden Markov Gaussian process with finite mean and finite variance, i.e.\@
        $\truem \coloneqq \sum_{k=1}^{\infty} \red{\pi_k} N\left(\red{\mu_t}, \red{\Sigma_t}\right)$ where
        $\mathrm{eig}(\Sigma_t) > C_1 > 0$ for some global constant $C_1$. 
        Let $\red{T} \to \infty$, $\epsilon_T \coloneqq \sqrt{\log(\red{T})} / \sqrt{\red{T}}$ and $h(\cdot)$ be
        Hellinger distance. 
        There is a global constant $C_2$ such that the posterior over the transition densities constructed and the
        true transition density satisfies the following. 
    
        \begin{equation*}
            \Pr_{\truem}\left( \sup_{x_{t-1}} h(\red{p_{0\,\,x_{t} \vert x_{t-1}}}, \green{q_{T\,\, x_t \vert
            x_{t-1}}}) \geq C_2 \epsilon_T \mvert X\right)  \to 0 
        \end{equation*}
    
    \end{theorem}

    \vfill \hfill \hyperlink{ContractionRates}{\beamerbutton{Return}}

\end{frame}

\begin{frame}[label=MarginalContractRateThm, noframenumbering, plain]{Marginal Distribution Contraction Rate}

    \begin{theorem}
        Let $\truem$ be a Hidden Markov Gaussian process, i.e.\@ $p_0 \coloneqq \sum_{k} \red{\pi_{k,t}}
        N\left(\red{\mu_t}, \red{\Sigma_t}\right)$ with finite mean and finite variance. 
        Let $\red{\Sigma_t}$ be a sequence of covariance matrices satisfies $\mathrm{eig} (\red{\Sigma_t}) > C_1 >
        0$ for some global constant $C_1$. 
        Let $\red{T} \to \infty$, then the following holds, with $\epsilon_T =
        \sqrt{\frac{\log(\red{T})}{\red{T}}}$, then there is a global constant $C$ such that the
        posterior induced the model studied here satisfies the following. 

            \begin{equation*}
                \Pr_{\truem}\left(h\left(\red{p_{0}}, \blue{q_{T}}\right) \geq C_3 \sqrt{\log(\red{T})} /
                \sqrt{\red{T}} \mvert X\right)  \to 0 
            \end{equation*}

    \end{theorem}

    \vfill \hfill \hyperlink{ContractionRates}{\beamerbutton{Return}}
\end{frame}

\begin{frame}[noframenumbering, plain]{1-Period Conditional Forecasts: VAR(1)}

    \begin{tabularx}{\textwidth}{Y Y}
        Unemployment rate & House supply \\
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures_cdpm/UNRATE_forecasts_var} &
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures_cdpm/HSUPPLY_forecasts_var} \\ 
        Inflation  & Consumption Expenditure \\ 
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures_cdpm/INFLA_forecasts_var} &
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures_cdpm/RCONSMP_forecasts_var} \\
    \end{tabularx}

\end{frame}



\begin{frame}[noframenumbering, plain]{1-Period Conditional Forecasts: VAR(1)}

    \begin{tabularx}{\textwidth}{Y Y}
            \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_cdpm/UNRATE_pit_var.pdf} & 
            \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_cdpm/UNRATE_acf_var} \\ 
            \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_cdpm/INFLA_pit_var} & 
            \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_cdpm/INFLA_acf_var} \\
            \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_cdpm/HSUPPLY_pit_var} & 
            \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_cdpm/HSUPPLY_acf_var} \\
            \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_cdpm/RCONSMP_pit_var} & 
            \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_cdpm/RCONSMP_acf_var} \\ 
    \end{tabularx}

\end{frame}


\begin{frame}[noframenumbering, plain]{Empirical Example: High-Frequency Data}

    \begin{itemize}
        \item Data: Four Daily series. (Jan/04/2005 -- Dec/31/2014)
        \begin{itemize}
            \item XLF
            \item VFH
            \item VNQ
            \item OIL %(percent change from preceding period)
        \end{itemize}
    \end{itemize}

    \begin{center}
        \includegraphics[width=.7\textwidth]{figures_hf/data_series_hf}
    \end{center}

\end{frame}

\begin{frame}[noframenumbering, plain]{What Do the Estimates Teach Us?}
    \begin{tabularx}{\textwidth}{Y Y}
        Mean & Innovation Volatility \\
        \includegraphics[width=.49\textwidth, height=.25\textheight]{figures_hf/means_data_hf} &
        \includegraphics[width=.49\textwidth, height=.25\textheight]{figures_hf/innov_vol_hf} \\ 
        Coefficient volatility & Number of Clusters \\
        \includegraphics[width=.49\textwidth, height=.25\textheight]{figures_hf/beta_vol_hf} &
        \includegraphics[width=.49\textwidth, height=.25\textheight]{figures_hf/cluster_id_hf} \\
    \end{tabularx}

    \footnotesize

    \begin{itemize}
        \item Coefficient dynamics changes more / Smaller change in innovation volatility.
        \item The Mean tracks the data very well.
        \item More Clusters are requigreen in recessions.
    \end{itemize}
\end{frame}

\begin{frame}[noframenumbering, plain]{What Do the Estimates Tell Us?}

    \begin{center}
        \includegraphics[width=.5\textwidth]{figures_hf/smoothed_posterior_cluster_probs_hf}
    \end{center}

    \begin{itemize}
        \item Smoothed cluster probabilities support non-Gaussianity.
        \item Given the stationary cumulative distribution of clusters, 10 clusters cover about 80\%.
        \item Can we interpret each cluster? 
    \end{itemize}

\end{frame}


\begin{frame}[noframenumbering, plain]{1-Period Conditional Forecasts: CDPM}

    \vfill
    \begin{tabularx}{\textwidth}{Y Y}
        XLF  & VNQ \\
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures_hf/XLF_forecasts_hf} &
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures_hf/VNQ_forecasts_hf} \\ 
        VFH & OIL \\  
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures_hf/VFH_forecasts_hf} &
        \includegraphics[width=.49\textwidth, height=.35\textheight]{figures_hf/OIL_forecasts_hf} \\
    \end{tabularx}

\end{frame}



\begin{frame}[noframenumbering, plain]{1-Period Conditional Forecasts: CDPM}

    \begin{tabularx}{\textwidth}{Y Y}
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_hf/XLF_pit_hf} & 
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_hf/XLF_acf_hf} \\ 
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_hf/VFH_pit_hf} & 
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_hf/VFH_acf_hf} \\
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_hf/VNQ_pit_hf} & 
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_hf/VNQ_acf_hf} \\
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_hf/OIL_pit_hf} & 
        \includegraphics[width=.49\textwidth, height=.2\textheight]{figures_hf/OIL_acf_hf} \\ 
    \end{tabularx}

\end{frame}

\begin{frame}[label=algorithm]{Gibbs Sampler}
    \begin{itemize}
        \item \textbf{Posterior of $\delta$}
        \begin{itemize}
        \item[-] Use \textcite{walker2007sampling} to determine the number of clusters $K$.
            \item[-] Given $K$, use multinomial sampling 
to draw $\delta_t$ with probability $p_1,\ldots, p_k$ where

\begin{equation*}
    p_k \propto \frac{1}{\Sigma_k} \phi \left(\frac{x_t - \beta_k x_{t-1}}{\Sigma_k} \right) + \text{Prior($k$),
    \quad } \forall k 
\end{equation*}

        \end{itemize}
        \item \textbf{Posterior of $\Pi$}
        \begin{itemize}
            \item[-] Draw from the conditional distribution of clusters given $\delta_{t-1}$.
            \item[-] Estimate the posterior of $\Pi$, denoted by $\Pi^{post}$,
                \[
                    \Pi_{kj}^{post} = \frac{\Pr(\delta_{t-1}=k) \Pr(\delta_t = j) + \#(\text{transitions\,\,} k
                    \rightarrow j)}{\Pr(\delta_{t-1}=k) + \sum_j \# (\text{transitions\,\,} k \rightarrow j)}
                \]
        \end{itemize}
        \item \textbf{Bayesian regression}
        \begin{itemize}
            \item[-] Given each cluster $k$, use regression to estimate $(\beta_k, \Sigma_k)$.
        \end{itemize}
        \item \textbf{Iterate} \quad \hyperlink{eststrategy}{\beamerbutton{Return}}
    \end{itemize}
\end{frame}

\end{document}
