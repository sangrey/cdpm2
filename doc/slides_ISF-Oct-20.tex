\documentclass[11pt,handout, xcolor=svgnames]{beamer}
\usepackage{dirichlet_slides}

\title[Minsu Chang and Paul Sangrey]{Feasible Multivariate Density Estimation Using Random Compression}

\author{Minsu Chang\inst{1} \and Paul Sangrey \inst{2}}  
\institute{\inst{1} Georgetown University \and \inst{2}Amazon}

\date{October 26, 2020}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

\begin{frame}[plain]
    \maketitle
\end{frame}

\addtocounter{framenumber}{-1}


\begin{frame}{Estimating Conditional Densities} 

    \begin{itemize}
        \item A classic problem across econometrics, statistics, and machine learning.
            \vspace{.75\baselineskip}
        \item Parametric assumptions are often restrictive and models are often sensitive to deviations from these assumptions.
            \vspace{.75\baselineskip}
        \item For $x_t \in \R^D$, $t = 1, \ldots, T$, nonparametric conditional densities often converge very slowly when $D$ is large.
            \vspace{.75\baselineskip}
        \item We can't avoid this \emph{Curse of dimensionality}
            \pause
            \vspace{.75\baselineskip}
%
        \item Instead:
            \begin{enumerate}
                \item  We develop an estimator that converges \emph{fast with high probability} by extending ideas from the random compression literature.
                \vspace{.5\baselineskip}
            \item We develop a computationally efficient Bayesian sampling algorithm to produce parsimonious nonparametric density estimates for a large number of series.
        \end{enumerate}
    \end{itemize}

\end{frame}

%
\begin{frame}{Data Generating Process}

    \begin{itemize}

    \item We estimate conditional densities for $x_t \in \R^D$, $t = 1, \ldots, T$: 
        \begin{equation*} 
            \truem\left(x_t \mvert \F_{t-1}\right) \coloneqq \sum_{k=1}^{\infty} \Pi_{t-1, k}^{p} \phi\left(x_t \mvert x_{t-1} \beta_{k,t}, \Sigma_{k,t}\right)
        \end{equation*}
%
    \begin{enumerate}
        \item  The $\truem$ are infinite Gaussian mixtures for each $T$, the $\mu_T$ are uniformly bounded, and the $\Sigma_{t}$ are positive definite.
            \vspace{.5\baselineskip}
            \begin{itemize}
                \item This holds if $\truem$ has any absolute moments. It allows for multi-modality, skewness, and fat-tails.
            \end{itemize} 
%
            \vspace{\baselineskip}
%
        \item There exists a (potentially constant) $z_t$ such that $(x_t', z_t')'$ is a first-order geometric Markov process.
%
    \end{enumerate}
\end{itemize}
\end{frame}
%
%
%
\begin{frame}
\frametitle{Contributions}
    \begin{enumerate}
        \item We construct an approximating mixture process
        \begin{equation*} 
            \approxm\left(x_t \mvert \F_{t-1}\right) \coloneqq \sum_{k=1}^{\infty} \Pi_{t-1, k}^{q} \phi\left(x_t \mvert x_{t-1} \beta_{k}, \Sigma_{k}\right) 
        \end{equation*}
%
        \vspace{.5\baselineskip}
        \begin{itemize}
            \item Its number of components \blue{$K_T$} grows logarithmically with \red{$T$}.
            \item This rate only depends on the number of series $D$ up to a constant.
        \end{itemize} 
%
        \vspace{.5\baselineskip}
        \item We provide a computationally efficient Bayesian estimator \estm for \truem's  marginal and
            transition densities.  
            \vspace{.5\baselineskip}
        \begin{itemize}
            \item It converges at a rate $\sqrt{\log(\red{T})}/\sqrt{\red{T}}$ with high probability.
        \end{itemize} 
%
        \vspace{.5\baselineskip}
        \item  We show $\estm$ performs well in simulation and an empirical examples.
    \end{enumerate}
\end{frame}

\section{Random Compression}

\begin{frame}{What does \estm\ converges \emph{fast  high probability} mean?}

    \begin{itemize}
        \item We take an external source of randomness (such as a prior), and use it to compress the data.
%
        \item Given $\delta > 0$, with probability  $1- 2 \delta$ with respect to this randomness, the implied representation only uses $C \log(T)$  terms.
%
        \item Since the number of mixture components only grows logarithmically, we can construct an estimator with convergence rate $\sqrt{\log(T) / T}$ with probability $1-2 \delta$.
    \end{itemize}

\end{frame}

\begin{frame}{Relationship to Data Compression}
    \begin{itemize}
    \item We can construct a random compression operator $\Theta_T$ that bins the data.
%
    \item The random compression literature %TODO ADD citations, 
        lets us show that if we apply $\Theta_T$ to the rescaled $T D$-dimensional data $X_T$, we can get a compressed $K_T D $ vector $X_T^{\ast}$ where the norms of $X_t$ and $X_t^{\star}$ are uniformly close, and $K_T \propto \log(T)$.
%
    \item The Gaussian kernel is $\exp(-1/2 (x_t - \mu_t) \Sigma_T^{-1} (x_t - \mu_t)')$. If the  weighted squared norms $(x_t - \mu_t) \Sigma_T^{-1} (x_t - \mu_t)$ don't move much for all $t$ during the compression, the distributions will not either.
%
    \item This lets us represent the joint density of the data.
%
    \item We use the joint distribution representation to derive representations for the transition and marginal distributions.

\end{itemize}



\end{frame}


\section{Estimation}


\begin{frame}{Estimation Strategy}

    Estimator:
        \begin{equation*} 
            \approxm\left(x_t \mvert \F_{t-1}\right) \coloneqq \sum_{k=1}^{\infty} \Pi_{t-1, k}^{q} \phi\left(x_t \mvert x_{t-1} \beta_{k}, \Sigma_{k}\right) 
        \end{equation*}

    \begin{itemize}
        \item We show the Dirichlet Process acts is a compression operator like the one we constructed above.
        \vspace{.5\baselineskip}
    \item In the i.i.d.\@ case, our estimator is just a Dirichlet mixture model. 
        \vspace{.5\baselineskip}
    \item \textbf{Complication:} Estimating transition distributions requires that $\Pr(\text{component}_t = k | \text{component}_{t-1})$ vary over time.
        \vspace{.5\baselineskip}
    \item[$\implies$] 
        We must construct a prior for \blue{$\Pi^q_{\kappa, k}$} for all $\kappa$ and $k$.
    \end{itemize}

\end{frame}


\begin{frame}{Bayesian Estimation}

    \begin{equation*}
        \approxm\left(x_t \mvert x_{t-1}, \blue{\kappa} \right) = \sum_{k=1}^{\blue{K_T^2}}
        \blue{\Pi^q_{\kappa, k}} N(\blue{\beta_{k}} x_{t-1}, \blue{\Sigma_{k}}). 
    \end{equation*}
 
    \textbf{Prior on $\blue{\Pi^q_{\kappa, k}}$}
    \vspace{.5\baselineskip}
    \begin{itemize}
        \item We use a Dirichlet process prior period-by-period to allow $\blue{K_T}$ to take any value.
            \vspace{.5\baselineskip} 
        \item By stacking the Dirichlet processes over time, we obtain a Dirichlet process over the
            $\blue{(k, \kappa)}$ product space.  
            \vspace{.5\baselineskip}
        \item The stacked Dirichlet processes define a prior for \blue{$\Pi^q_{\kappa, k}$}. 
    \end{itemize}

\end{frame}

\begin{frame}[c]{Hierarchical Prior on Coefficients}

 Some of the components only have a few datapoints in them. We use a hierarchical prior to increase the estimator's efficiency.

 \begin{gather*}
%
     \lbrace \beta_k \rbrace_{k=1}^K \vert \Sigma_k, \bar{\beta}, U \sim \text{Matrix-Normal}\left(\bar{\beta}, \Sigma_k, U\right) \\  \\
%
     \lbrace \Sigma_k \rbrace_{k=1}^K \vert \bar{\Sigma} \sim \text{Inverse-Wishart} \left(\Psi, \mu_1 + D - 1 \right)  \\ \\
 %
     \bar{\beta}, U \sim \text{Matrix-Normal}(B, \mathbb{I}_D, U) \text{Inverse Wishart}(\Psi_U, \nu_U)  \\ \\
%
     \bar{\Sigma} \sim \text{Wishart}\left(\frac{\mathrm{diag}(a_1, \ldots, a_D)}{\mu_2 + D -1}, \mu_{2} + D - 1 \right) 
 \end{gather*}
\end{frame}

\begin{frame}{Asymptotic Properties}

    \begin{itemize}
        \item Given $\delta > 0$, the posterior for $\approxm$ contracts towards $\truem$ at a rate $\sqrt{\log(T) / T}$ with probability at least $1 - 2 \delta$ with respect to the prior and uniformly with respect to $\truem$.
            \vspace{\baselineskip}
        \item This holds for the estimators for both the marginal and transition distributions.
    \end{itemize}


\end{frame}

\begin{frame}{Bayesian Estimation}

    \textbf{Posterior of $\green{k_1, k_2, \ldots k_T}$:}

    \begin{itemize}
        \item For each period, we use multinomial slice sampling to draw $\green{k}$, \\
            Walker (2007).
        \item Update the marginal probabilities for the $k$.
    \end{itemize}
        \vspace{.5\baselineskip}

    \textbf{Posterior of \green{$\pi$}:}

   \begin{itemize}
        \item We draw from the conditional distribution of clusters given \green{$\kappa$}.
            \vspace{.5\baselineskip}
        \item We can draw \green{$\Pi^q_{\kappa, k}$} conditional on its stationary distribution by 
            \begin{equation*}
                \green{\Pi\left(j \mvert h\right)} = \frac{\blue{\pi^{prior}(\kappa = h)\pi^{prior}(k=j)} + \blue{\#(\text{transitions}\ h \to j)}}{\blue{\pi^{prior}(\kappa=h)} + \sum_{i=1}^{\blue{K_T^2}} \blue{\#(\text{transitions}\ h \to i)}} 
            \end{equation*}
    \end{itemize}
%
    \textbf{Bayesian Regression:}
    
    \begin{itemize}
        \item Given each cluster $\green{k}$, we use regression to estimate \green{$(\beta_k, \Sigma_k^2)$}.
    \end{itemize}
        \vspace{.5\baselineskip}

    \textbf{Iterate:}
\end{frame}

\section{Results}

\begin{frame}{Simulation Results}
    \begin{itemize}
        \item We simulate a bivariate vector autoregression with $t$-distribution shocks.
        \item We impose a fairly flat prior.
    \end{itemize}
    %    
\begin{figure}[htb]
    \caption[PIT Histogram and Autocorrelation Function (ACF)]{PIT Histogram and Autocorrelation Function (ACF)}
%
    \label{fig:simulation_pit_histacf}
%
    \begin{subfigure}[t]{.45\textwidth}
        \includegraphics[width=\textwidth, height=1.5in]{figures/simulation_vart_cdpm/Var1_vart_pit_fin} 	
        \caption{First Variable}
    \end{subfigure}
%  
    \hfill
%
    \begin{subfigure}[t]{.45\textwidth}
        \includegraphics[width=\textwidth, height=1.5in]{figures/simulation_vart_cdpm/Var2_vart_pit_fin}
        \caption{Second Variable}
    \end{subfigure}
  %
\end{figure}


\end{frame}

\begin{frame}{Empirical Example: CDPM}

    Data:
    \vspace{.5\baselineskip}

    \begin{tabularx}{\textwidth}{l l}
        1) Unemployment Rate       & 4) Inflation  \\
        2) Housing Supply          & 5) Real Personal Consumption \\
        3) Industrial Production   & 6) Bond Yields
   \end{tabularx}

\begin{figure}[htb]

  \caption{One-Period-Ahead Conditional Forecasts: Consumption } 
  \label{fig:consump_forecasts}

  \begin{subfigure}[t]{.4\textwidth}
    \centering
    \includegraphics[width=.95\textwidth, height=1.5in]{figures/empirics_cdpm/Consumption_forecasts_fin} 
    \caption{Posterior Density}
    \label{fig:ConsumpPostDens}
  \end{subfigure}
%
  \begin{subfigure}[t]{.29\textwidth}
    \centering
    \includegraphics[width=.95\textwidth, height=1.5in]{figures/empirics_cdpm/Consumption_pit_fin} 
    \caption{PIT Histogram}
    \label{fig:ConsumpmarginalmattHist}
  \end{subfigure}
%
  \begin{subfigure}[t]{.29\textwidth}
    \centering
    \includegraphics[width=.95\textwidth, height=1.5in]{figures/empirics_cdpm/Consumption_acf_fin} 
    \caption{PIT ACF}
    \label{fig:ConsumpmarginalmattAcf}
  \end{subfigure}

\end{figure}

\end{frame}
%
 
\section{Conclusion}
 
\begin{frame}[c]{Conclusion}

    \begin{itemize}
        \item  Adapted ideas from the Random Compression Literature to develop a conditional distribution estimator that converges \emph{fast with high probability}.
            %
        \item Our nonparametric estimators converge at $\sqrt{\log(T) / T}$ rate with probability $ 1 - 2 \delta$ with respect to the randomness introduced by the compression.
        %
        \item Developed a parsimonious Bayesian mixture distribution estimator for transition distributions of Markov data based upon Dirichlet process that can be estimated in only a few minutes on standard hardware.
        %
        \item Showed  our estimator works well in simulation and in a monthly macroeconomic panel.
    \end{itemize}

\end{frame}
\end{document}
