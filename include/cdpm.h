#pragma once

#include "metropolis.h"
#include <arma_wrapper.h>
#include <armadillo>
#include <algorithm>
#include <cmath>
#include <functional>
#include <limits>
#include <memory>
#include <optional>
#include <random>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <tuple>
#include <utility>
#include <unordered_map>
#include <variant>
#include <vector>


namespace cdpm {

using namespace std::string_literals;
using namespace met;
using aw::npulong;
using aw::npdouble;
using aw::dmat;  
using aw::dcube;
using aw::iuvec; 
using dvec = arma::Col<npdouble>;
using drow = arma::Row<npdouble>; 
using var_t = std::variant<std::vector<double>, npdouble, bool, dmat>;
using vec_map = std::unordered_map<std::string, var_t>; 
using vec_nested_map = std::unordered_map<std::string, std::variant<vec_map, npdouble>>;
using std::cbegin;
using std::cend;

/*Forward declare several variables.*/
class cdpm;
void forecast_regressor(cdpm&); 
void forecast_regressand(cdpm&);
void check_valid_model(const cdpm& model); 

/* These lines create a deduction guide that is used to determine ho the std::visit should handle overloads. */
template<typename T> struct always_false : std::false_type {};
template<typename... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<typename... Ts> overloaded(Ts...) -> overloaded<Ts...>;

/*
 * This exception class is defined to be used in a manner similar to std::invalid_argument, except that it is used
 * on class methods where the values of the class members are invalid. Basically, it is an invalid_argument 
 * exception where the invalid_argument it *this.
 */
class invalid_this_argument : public std::invalid_argument  {

    using std::invalid_argument::invalid_argument;
};

dmat extract(const vec_map& prior, const std::string& name) {
   
    auto find_ptr = prior.find(name);
    
    if (find_ptr == prior.end())  {
    
        throw std::invalid_argument("The inner prior does not contain an value named " + name + "."); 
    } 

    dmat returnmat = std::visit(overloaded {
            [] (const dmat& arg) {return arg;},
            [] (const bool arg) {return dmat({double(arg)});},
            [] (const double arg) {return dmat({arg});},
            [] (const std::vector<double>& arg) {return arma::conv_to<dmat>::from(arg);},
            [] (const auto& arg) {return arma::conv_to<dmat>::from(arg);},
            }, prior.at(name));

    return returnmat;
}

vec_map extract(const vec_nested_map& prior, const std::string& name) {
   
    auto find_ptr = prior.find(name);
    
    if (find_ptr == prior.end())  {
    
        throw std::invalid_argument("The top-level prior does not contain an value named " + name + "."); 
    } 
    else {

        return std::get<vec_map>(prior.at(name));

    }   


}

class coeff_prior {
    
    public:
        dmat mean;
        dmat covariance;

        /* 
         * Create a function to draw from the prior over the component coefficients. 
         */
        virtual dmat draw(bool stationary=false, npulong num_stationary_tries=100) const  = 0; 
        virtual void fit(const dcube& matrices, const dcube& covariances)  = 0; 
        virtual ~coeff_prior() = default;

        /* 
        * Create a function to draw from the prior over the component coefficients. 
        */
        static dmat draw_gaussian_coef(const dmat& mean_, const dmat& cov_, bool
                stationary=false, npulong num_stationary_tries=100) { 
         
            dmat returnmat(arma::size(mean_)); 
            std::size_t loop_counter = 0;

            /* Enforce stationarity */
            do {

                dmat innovations = arma::reshape(met::randn(returnmat.n_elem), arma::size(mean_)); 
        
                returnmat = mean_ + lt_root_mat(cov_) * innovations; 
        
                ++loop_counter;
                if(loop_counter > num_stationary_tries) {
                    throw std::runtime_error("The values you passed in are too far from stationary. "
                                             "I cannot find a stationary matrix in the support of the prior. ");
                }
        
            } while (stationary && !is_stationary(returnmat)); 
        
            return returnmat; 
        
        } 
     
};


class hierarchical_coeff_prior final : public coeff_prior {

    public:
        /* The hyperparamters. I use mean and covariance from the base class as the updated paramters. */
        dmat hypermean;
        dmat hyper_scale;
        double df;

        hierarchical_coeff_prior() = default; 
        hierarchical_coeff_prior(const vec_map& prior) {

            /*
             * First I initialize the covariance.
             */
            covariance = reshape_square(extract(prior, "cov")); 
            size_t regressor_dim = covariance.n_rows;

            /*
             * I Then initialize the degrees of freedom.
             */
            auto df_ptr = prior.find("nu");
            if (df_ptr != prior.end())  {

                std::visit(overloaded {
                        [this, regressor_dim] (double arg) {this->df = arg + regressor_dim -1;},
                        [this, regressor_dim] (dvec arg) {this->df = arg.at(0) + regressor_dim - 1;},
                        }, df_ptr->second);

            } else {
                
                /* 
                 * df = 2 + regressor_dim -1 would imply that the correlations are U[-1,1]. 
                 * However, then the mean would not be well-defined.
                 * So we use regressor_dim + 2, this is rather general, but the mean is well-defined. 
                 */
                df = 1 + regressor_dim + 1; 

            }

            /* I must have at least one more degree of freedom than I do dimensions. */
            if(df <= (regressor_dim + 1)) {
                throw std::invalid_argument("The first degrees of freedom parameter (df: "s 
                        + std::to_string(df) + ") must be greater than the dimension (dim:"s + std::to_string(regressor_dim) 
                        + ") + 1."s); 
            }

            covariance = reshape_square(extract(prior, "cov")); 
                 
           if (arma::any(covariance.diag() < 0)) {

               throw std::invalid_argument("The covariance must have a strictly positive diagonal."); 
           }

           /*
            * I set the hyperscale so that the covariance the mean is the value given.
            */
           hyper_scale = covariance * (df - regressor_dim - 1);

           /*
            * Then I setup the hypermean.
            */
           dmat mean_in = extract(prior, "mean"); 

           if ((mean_in.n_elem % regressor_dim) != 0) {

               throw std::invalid_argument("The prior coefficient mean (n_elem: "s + 
                       std::to_string(mean_in.n_elem) + ") is not the compatible with the number "s + 
                       "of regressor colums  implied by the covariance ("s + std::to_string(regressor_dim) + ")."s); 
           }
           size_t regressand_dim = mean_in.n_elem / regressor_dim;

           hypermean = arma::reshape(mean_in, regressor_dim, regressand_dim); 

           /*
            * I end by initializing the mean.
            */
           mean = draw();
        }

        /*
         * Setup a way of drawing from the prior.
         */
        dmat draw(bool stationary=false, npulong num_stationary_tries=100) const  final { 
        
            return coeff_prior::draw_gaussian_coef(hypermean, covariance, stationary, num_stationary_tries);  

        }

        void fit(const dcube& matrices, const dcube& covariances) final {

            if ((matrices.n_rows != hypermean.n_rows) || (matrices.n_cols != hypermean.n_cols)) {
                throw std::invalid_argument("The matrices do not have a valid shape."s 
                        + " The matrices.n_rows is "s + std::to_string(matrices.n_rows) + " but should be "s 
                        + std::to_string(hypermean.n_rows) + " . The matrices.n_cols is "s 
                        + std::to_string(matrices.n_cols) + " but should be "s  
                        + std::to_string(hypermean.n_cols) + "."s);

            }

            if ((covariances.n_rows != hypermean.n_cols) || (covariances.n_rows != covariances.n_cols)) {
                throw std::invalid_argument("The covariances do not have a valid shape."s 
                        + " The covariances.n_rows is "s + std::to_string(covariances.n_rows) 
                        + ", and its n_cols is "s + std::to_string(covariances.n_cols) + "but it should be "s 
                        + std::to_string(hypermean.n_cols) + "."s);
            }

            if (covariances.n_slices != matrices.n_slices) {
                throw std::invalid_argument("Thie covariances and matrices do not have the same number of slices.");

            }

            /*
             * I start by updating the covariance.
             */
            covariance = draw_hetero_innov_cov(matrices, covariances, hypermean, hyper_scale, df); 
            
            /*
             * Then I update the mean.
             */
            auto [precision_factored, post_mean]  =
                met::matrix_heteroskedastic_mean_moments(matrices, covariances, hypermean);
            dmat innovations = arma::reshape(randn(mean.n_elem), arma::size(mean)); 
            mean = post_mean + lt_root_mat(covariance) * cho_solve(precision_factored, innovations.t()).t(); 

        }
};


class simple_coeff_prior final : public coeff_prior {

    public:
        
        simple_coeff_prior() = default;
        simple_coeff_prior(const vec_map& prior) {

                covariance = reshape_square(extract(prior, "cov")); 
                size_t regressor_dim = covariance.n_cols;
                if (covariance.n_cols != regressor_dim) {

                    throw std::invalid_argument("The prior coefficient covariance (n_cols: "s + 
                            std::to_string(covariance.n_cols) 
                            + ") is not the compatible with the number "s 
                            + "of regressor colums ("s + std::to_string(regressor_dim) + ")."s); 
                }

                dmat mean_in = extract(prior, "mean"); 

                if ((mean_in.n_elem % regressor_dim) != 0) {

                    throw std::invalid_argument("The prior coefficient mean (n_elem: "s + 
                            std::to_string(mean_in.n_elem) + ") is not the compatible with the number "s + 
                            "of regressor colums ("s + std::to_string(regressor_dim) + ")."s); 
                }

                npulong regressand_dim = mean_in.n_elem / regressor_dim;
                mean = arma::reshape(mean_in, regressor_dim, regressand_dim); 

        }

        /* Return a draw from the prior. */
        dmat draw(bool stationary=false, npulong num_stationary_tries=100) const  final { 
        
            return coeff_prior::draw_gaussian_coef(mean, covariance, stationary, num_stationary_tries);  

        }

        /* We do not update the mean and covariance here. */
        void fit([[maybe_unused]] const dcube& matrices, [[maybe_unused]] const dcube& covariances) final { }
};




class cov_prior {
    
    public:
        /* 
         * The default value for the degree of freedom parameter is 3 because 2 induces uniform distributions
         * on the correlations. (Here I assume dim() == 1), but 3 also gives a mean.
         */
        double mu1 = 3;
        /* This value gives the standard deviations a half-t prior in the univariate case.*/
        double mu2 = .5;

        /*
         * This should be the expected variance.
         */
        dvec scale_params;

    public:

        cov_prior() = default;
        virtual ~cov_prior() = default;
        cov_prior(const vec_map& prior) {
        
            /* Parameterize the distribution in terms of variances. */
            scale_params = extract(prior, "scale"); 
    
            auto mu1_ptr = prior.find("mu1");
            if (mu1_ptr != prior.end())  {

                std::visit(overloaded {
                        [this] (double arg) {this->mu1 = arg + dim() -1;},
                        [this] (dvec arg) {this->mu1 = arg.at(0) + dim() - 1;},
                        }, mu1_ptr->second);

            } else {
                
                /* 
                 * mu1 = 2 + dim() -1 would imply that the correlations are U[-1,1]. 
                 * However, then the mean would not be well-defined.
                 * So we use dim() + 2, this is rather general, but the mean is well-defined. 
                 */
                mu1 = 1 + dim() + 1; 

            }

            /* I must have at least one more degree of freedom than I do dimensions. */
            if(mu1 <= dim() + 1) {
                throw std::invalid_argument("The first degrees of freedom parameter (mu1: "s 
                        + std::to_string(mu1) + ") must be greater than the dimension (dim:"s + std::to_string(dim()) 
                        + ") + 1."s); 
            }

            auto mu_ptr = prior.find("mu2");
            if (mu_ptr != prior.end())  {

                std::visit(overloaded {
                        [this] (double arg) {this->mu2 = arg;},
                        [this] (dvec arg) {this->mu2 = arg.at(0);},
                        }, mu_ptr->second);

            } 

            if(mu2 <= 0) {
                throw std::invalid_argument("The second degrees of freedom parameter (mu2: "s 
                        + std::to_string(mu2) + ") must be positive."); 
            }
            
    }


    dmat draw_hyperprior_scale() const { 

        dmat scale_in = arma::zeros(dim(), dim());
        double df = mu2 + dim() - 1;
        scale_in.diag() = scale_params / df;
            
        thread_local auto& generator = initialize_mt_generator();
        met::wishart_distribution dist(df, scale_in);  
        
        return dist(generator);
    
    }

    size_t dim() const {
        return scale_params.n_elem;
    }

    size_t df() const {
        return mu1;
    }

    virtual dmat scale(size_t) = 0;
    virtual dcube scales() const = 0;
    virtual void fit(const dcube& covariances)  = 0;
    virtual void increase_cluster_dim(size_t) {};  
    virtual void reduce_cluster_dim(size_t) {}
    virtual void swap_clusters(size_t, size_t) {}

    /*
     * This function takes a draw from the prior.
     */
    dmat draw() const {
        
        dmat hyperprior_scale =  draw_hyperprior_scale(); 
        thread_local auto& generator = initialize_mt_generator();
        met::inverse_wishart_distribution dist(mu1, hyperprior_scale);  

        return dist(generator); 

    }

    /*
     * This function takesa draw from the prior conditional on the prior_scale (Psi)
     */
    dmat cond_draw(size_t h) {
        
        thread_local auto& generator = initialize_mt_generator();
        met::inverse_wishart_distribution dist(mu1, scale(h));  

        return dist(generator); 
    }
        
};


class cov_prior_hierarchical final : public cov_prior {
    
    public:
        dmat scale_member;

    public:

        cov_prior_hierarchical() = default;
        cov_prior_hierarchical(const vec_map& prior) : cov_prior{prior} {
        
            scale_member = arma::diagmat(scale_params);

        }

        dmat scale() const {
            return scale_member;
        }   

        dcube scales() const {
            return met::Cube(scale_member);
        }   
        
        dmat scale([[maybe_unused]] size_t) {
            return scale();
        }   

        
        void fit(const dcube& covariances) {

            if ((covariances.n_rows != dim()) || (covariances.n_cols != dim())) {

                throw std::invalid_argument("The covariances do not have the correct shape."s 
                        + "It must have "s + std::to_string(dim()) + " columns and "s + std::to_string(dim()) 
                        + " rows. Not "s + std::to_string(covariances.n_rows) + " rows and "s 
                        + std::to_string(covariances.n_cols) + " columns."s 
                        + " The number of slices is the number of periods."s);

            }

            double post_df = covariances.n_slices * (mu1 + dim() - 1) + (mu2 + dim() - 1);

            dmat identity = arma::eye(dim(), dim());

            dmat summed_precisions = arma::zeros(dim(), dim());

            for(size_t h=0; h<covariances.n_slices; ++h) {
                summed_precisions += arma::inv(covariances.slice(h));
                
            }

            summed_precisions  *= (mu1 - 2); 
            dmat inv_prior_scale = arma::diagmat(arma::pow(scale_params, -1));
            dmat post_scale = cho_solve(lt_root_mat(inv_prior_scale + summed_precisions), identity);

            thread_local auto& generator = initialize_mt_generator();
            met::wishart_distribution dist(post_df, post_scale);  

           scale_member = dist(generator);

        }

};

class cov_prior_F_dist final : public cov_prior {
    
    public:
        dcube scale_member;

    public:

        cov_prior_F_dist() = default;
        cov_prior_F_dist(const vec_map& prior) : cov_prior{prior} {
        
            /*
             * I start it with just one component.
             */
            scale_member.set_size(dim(), dim(), 1);
            scale_member.each_slice() = arma::diagmat(scale_params);

        }

        dcube scales() const {
            return scale_member;
        }   

        void increase_cluster_dim(size_t cluster_dim) {

            if (cluster_dim > scale_member.n_slices)  {
                
                size_t initial_dim = scale_member.n_slices;
                scale_member.resize(dim(), dim(), cluster_dim);
                /*
                 * I add slices until I get to cluster_dim.
                 */
                for(size_t k=initial_dim; k<cluster_dim; ++k) {
                    scale_member.slice(k) = draw_hyperprior_scale(); 

                }
                
            }
        }

        void reduce_cluster_dim(size_t cluster_dim) {
            if (cluster_dim < scale_member.n_slices) {

                scale_member.resize(dim(), dim(), cluster_dim);
            }
        }

        void swap_clusters(size_t k, size_t j) {
            scale_member.slice(k).swap(scale_member.slice(j));
        }

        dmat scale(size_t h) {
            
            this->increase_cluster_dim(h+1);

            return scale_member.slice(h);
        }   


        void fit(const dcube& covariances) {

            if ((covariances.n_rows != dim()) || (covariances.n_cols != dim())) {

                throw std::invalid_argument("The covariances do not have the correct shape."s 
                        + "It must have "s + std::to_string(dim()) + " columns and "s + std::to_string(dim()) 
                        + " rows. Not "s + std::to_string(covariances.n_rows) + " rows and "s 
                        + std::to_string(covariances.n_cols) + " columns."s 
                        + " The number of slices is the number of periods."s);

            }

            double post_df = covariances.n_slices * (mu1 + dim() - 1) + (mu2 + dim() - 1);

            dmat identity = arma::eye(dim(), dim());
            dmat inv_prior_scale = arma::diagmat(arma::pow(scale_params, -1));
            thread_local auto& generator = initialize_mt_generator();

            scale_member.set_size(dim(), dim(), covariances.n_slices);

            for(size_t h=0; h<covariances.n_slices; ++h) {

                dmat precision = (mu1 + dim() - 1) * arma::inv(covariances.slice(h));
                dmat post_scale = cho_solve(lt_root_mat(inv_prior_scale + precision), identity);
                met::wishart_distribution dist(post_df, post_scale);  
                scale_member.slice(h) = dist(generator);
                
            }

        }

};

class cdpm_prior {
    
    private:
        std::optional<double> expected_cluster_dim;

        static std::unique_ptr<cov_prior> construct_cov(const vec_map& cprior) {

            auto heirarchy_ptr = cprior.find("hierarchical");

            if ((heirarchy_ptr != cprior.end()) && std::get<bool>(heirarchy_ptr->second))  {
                return  std::make_unique<cov_prior_hierarchical>(cprior);

            } else {
                return  std::make_unique<cov_prior_F_dist>(cprior);
            }

        }

        static std::unique_ptr<coeff_prior> construct_coeff(const vec_map& cprior) {

            auto heirarchy_ptr = cprior.find("hierarchical");

            if ((heirarchy_ptr != cprior.end()) && std::get<bool>(heirarchy_ptr->second))  {
                return std::make_unique<hierarchical_coeff_prior>(cprior);

            } else {
                return std::make_unique<simple_coeff_prior>(cprior);
            }

        }

        static double construct_cluster_dim(double arg) {

            if (arg <= 0) {
                throw std::invalid_argument("The expected number of clusters must be positive, not "s 
                            + std::to_string(arg) + "."s);
            } 

            return arg;

        }

    public:
        std::unique_ptr<cov_prior> cov; 
        std::unique_ptr<coeff_prior> coeff; 

    public:

        cdpm_prior() = default;
        cdpm_prior(const vec_nested_map& prior, double expected_cluster_dim_arg) :
            cov{construct_cov(extract(prior, "cov"))}, 
            coeff{construct_coeff(extract(prior, "coeff"))} {
           
            this->expected_cluster_dim = construct_cluster_dim(expected_cluster_dim_arg);
        }

        cdpm_prior(const vec_nested_map& prior) :
            cov{construct_cov(extract(prior, "cov"))}, 
            coeff{construct_coeff(extract(prior, "coeff"))} {

            auto cluster_dim_ptr = prior.find("expected_cluster_dim"); 

            if (cluster_dim_ptr != prior.end()) {

                this->expected_cluster_dim = construct_cluster_dim(std::get<npdouble>(cluster_dim_ptr->second));
            }
        }

        double get_expected_cluster_dim() const {
            
            return expected_cluster_dim.value();

        }
        
        void set_expected_cluster_dim(double cluster_dim_arg) {

            assert (cluster_dim_arg > 0);
            expected_cluster_dim = cluster_dim_arg;

        }

        
};



/*
 * Draws the sticks from the observed counts and the prior weights. 
 */
dvec draw_sticks_from_counts(const arma::uvec& counts, double alpha) {

    assert(alpha > 0 && "The concentration parameter must be positive.");
    
    dvec sticks(counts.size()); 

    size_t data_dim = arma::sum(counts);

    for(size_t j=0; j<sticks.size(); ++j) {
        
       sticks(j) =  draw_beta(counts(j) + 1, data_dim - arma::sum(counts.head_rows(j+1)) + alpha); 

    }

    aw::assert_same_size(counts, sticks);

    return sticks;
}




/*
 * Compute the probabilties from the stick breaking representation. 
 */
dvec compute_probabilities(const dvec& sticks) {

    dvec prob_vec(sticks.size()); 
    
    prob_vec(0) = sticks(0); 
    double partial_prod = 1;
    
    for(size_t k=1; k<sticks.size(); ++k) {
        partial_prod *= (1 - sticks(k-1));
        prob_vec(k) = sticks(k) * partial_prod; 
    }
    
   return prob_vec;
}


/*
 * We draw the stick-breaking representation of the model using the Walker (2007) slice sampler. This is Part C in
 * his paper. The intuition is that we are using a Gibbs sampler, and since we are sampling from the marginal 
  distribtuion with respect to the Transition function, there is only one set of sticks to draw from.
 */
dvec draw_sticks(const iuvec& cluster_id, double expected_cluster_dim) { 

    size_t num_prev_comps = cluster_id.max() + 1;
    arma::uvec counts(num_prev_comps, arma::fill::zeros);

    for(const auto& val : cluster_id) {
        counts(val)++;
    }

    assert(counts.size() == num_prev_comps);
    dvec sticks = draw_sticks_from_counts(counts, expected_cluster_dim);

    assert(num_prev_comps == sticks.size());
    return sticks;

}


/* 
 * I Draw u using Walker (2007) 
 */
dvec draw_u(const iuvec& cluster_id, const dvec& probabilities, const dmat& trans_mat) {    

    dvec u_vec(cluster_id.size());
    thread_local auto& gen = initialize_mt_generator();
    
    // We set all events that  happen less than 1 in a million periods to zero.
    dmat trans_mat_in = arma::clamp(trans_mat, 1e-9, 1.0);

    for(size_t t=0; t<cluster_id.size(); ++t){  

       // If t = 0, we draw from the marginal distribution. 
        double prob = 1.0;
        if (t==0) {
            prob = probabilities(cluster_id(t));
        // Otherwise we draw from the marginal distribution.
        } else {
            prob = trans_mat_in(cluster_id(t),cluster_id(t-1)); 
        }
        /* std::cerr << "prob is " << prob << std::endl; */

       auto u_dist = std::uniform_real_distribution<double>(0.0, prob);
       u_vec(t) = u_dist(gen);

    }

   return u_vec;
}

/* 
 * Computes the probability vector for the components that have values realized in them. 
 */
inline std::tuple<dvec, dvec> compute_required_probabilities(const dvec& init_sticks, double u_star, double
        expected_cluster_dim) { 

    assert(arma::all(init_sticks > 0));
    assert(u_star > 0 && u_star < 1);
    assert(expected_cluster_dim > 0);

    /* 
     * We need to find the a k_star such that all of the datapoints are in the first k_star elements.
     * We draw w_j from its prior if we need more components.
     */
    auto temp_sticks = arma::conv_to<std::vector<double>>::from(init_sticks);
    size_t k_star = 0;
    std::vector<double> prob_vec;
    double partial_sum = 0;
    double w_partial_prod = 1;

    /*
     * Keep computing probabilities until we know we have enough components to place all of the periods in.
     */
    do {

        prob_vec.push_back(w_partial_prod);
 
        if (k_star >= temp_sticks.size()) { /* We add a new component */
             
            temp_sticks.push_back(draw_beta(1.0, expected_cluster_dim));
        }
        
        prob_vec.at(k_star) *= temp_sticks.at(k_star);
        partial_sum += prob_vec.at(k_star);
        w_partial_prod *= (1 - temp_sticks.at(k_star));
        ++k_star;

    } while (partial_sum <= (1 - u_star)); 

    temp_sticks.resize(k_star);

    dvec return_sticks = arma::conv_to<dvec>::from(temp_sticks);
    dvec return_probs = arma::conv_to<dvec>::from(prob_vec);

    /* Only new sticks can differ from the old sticks. */
    assert(prob_vec.size() == k_star);
    assert(std::equal(cbegin(return_sticks), cbegin(return_sticks) + std::min(k_star,
                    static_cast<size_t>(init_sticks.size())), cbegin(init_sticks)));
    assert(return_sticks.size() == prob_vec.size());
 
    return std::make_tuple(return_sticks, prob_vec); 
}

/*
 * We now turn to sampling the indicator variables.  This is Step D in Walker (2007)
 */
template<typename FunctionType>
std::tuple<iuvec, dvec>  draw_cluster_id(const iuvec& init_cluster_id, const dvec& init_sticks, const dmat&
        init_trans_mat, FunctionType& coeff_weight, double expected_cluster_dim) {

    assert(arma::all(init_sticks >= 0)); 
    assert(expected_cluster_dim > 0 && "The prior concentration must be positive.");

    dvec init_prob = compute_probabilities(init_sticks);
    dvec u_vec = draw_u(init_cluster_id, init_prob, init_trans_mat);

    /* We ignore things that happen less than one in a billion periods. */
    double u_star = std::max(u_vec.min(), 1e-9); 
    /* We now draw the new marginal distribution. */
    auto [sticks, prob_vec] = compute_required_probabilities(init_sticks, u_star, expected_cluster_dim);

    /* We never need more components than we have datapoints. */
    if (prob_vec.size() > init_cluster_id.size()) {
        prob_vec.resize(init_cluster_id.size());
    }

    size_t overlap_idx = std::min(static_cast<size_t>(prob_vec.size()), static_cast<size_t>(init_trans_mat.n_rows)) - 1; 
    
    dmat new_trans_mat = prob_vec * prob_vec.t();

    /* 
     * We now multiply and divide through so that the marginal distributions are correct. We only need to use the
     * overlapping columns because the new transition matrix is either not defined or just the prior the rest of
     * the time.
     */
    new_trans_mat.submat(0, 0, overlap_idx, overlap_idx) = init_trans_mat.submat(0, 0, overlap_idx, overlap_idx);
    new_trans_mat.submat(0,0, overlap_idx, overlap_idx) = new_trans_mat.submat(0, 0, overlap_idx,
            overlap_idx).each_col() % (prob_vec.head_rows(overlap_idx+1) / init_prob.head_rows(overlap_idx+1));
    new_trans_mat.submat(0,0, overlap_idx, overlap_idx) = new_trans_mat.submat(0, 0, overlap_idx,
            overlap_idx).each_col() % (init_prob.head_rows(overlap_idx+1) / prob_vec.head_rows(overlap_idx+1));

    iuvec cluster_id(init_cluster_id.size());
    
    thread_local auto& generator = initialize_mt_generator();

    /* 
     * Draws the component label from the valid components with weights governed by their the probability of
     * observing the dependent variable given that functions coefficients. 
     */
    for(size_t t=0; t<cluster_id.size(); ++t) {

        std::vector<double> weights(prob_vec.size());

        for(size_t k=0; k<prob_vec.size(); ++k) {

            if(t==0) {
                weights.at(k) = (prob_vec(k) > u_vec(t)) ? coeff_weight(t,k) : 0.0;
            } else {
                /* std::cerr << "new trans_mat is " << new_trans_mat << std::endl; */
                weights.at(k) = (new_trans_mat(k, cluster_id(t-1)) > u_vec(t)) ? coeff_weight(t,k) : 0.0;
            }

         }
         cluster_id(t) = std::discrete_distribution<npulong>(cbegin(weights), cend(weights))(generator);
 
    }
   
    /* If there are more sticks than are realized to have components in them, we throw out the unused sticks. */ 
    sticks.resize(cluster_id.max() + 1);
    assert(cluster_id.max() + 1 == sticks.size());

    return std::make_tuple(cluster_id, sticks);
}


/*
 * This function draws the component specific parameters from their conditional distribution. The problem is
 * more straightforward here because the only thing that depends on the cluster dimension is the cluster_id,
 * and that is known.  
 */ 
std::tuple<dcube, dcube> draw_component_specific_params(const iuvec& cluster_id, const dmat& regressor, const
        dmat& regressand, const cdpm_prior& prior) {

    aw::claim_same_n_rows(cluster_id, regressor, regressand);

    /* Set up some iniital variables. */ 
    const npulong cluster_dim = cluster_id.max() + 1;
    const npulong regressor_dim = regressor.n_cols;  
    const npulong regressand_dim = regressand.n_cols; 

    /* Specify preconditions. */
    aw::assert_is_finite(cluster_id, regressor, regressand); 
    aw::claim_same_n_rows(regressor, regressand, cluster_id); 

    dcube post_comp_cov = dcube(regressand_dim, regressand_dim, cluster_dim).fill(arma::datum::nan); 
    dcube post_beta = dcube(regressor_dim, regressand_dim, cluster_dim).fill(arma::datum::nan); 

    /* Compute some values for use later */
    auto sorted_indices = sort_indices(cluster_id); 

    for(npulong h=0; h<cluster_dim; ++h) { 

        arma::uvec component_indices = arma::conv_to<arma::uvec>::from(sorted_indices[h]);
        const npulong comp_dim = component_indices.size(); 

        /* 
         * We can use comp_dim == 0 because we are using Bayesian methods that shrink the prior and hence can
         * handle models where the number of degrees of freedom exceeds the number of datapoints without becoming
         * degeenrate. 
         */
        if (comp_dim > 0) { 

            const dmat& comp_regressor = regressor.rows(component_indices);
            const dmat& comp_regressand = regressand.rows(component_indices); 
            
            auto [beta, cov] = draw_coeff_and_cov(comp_regressor, comp_regressand, prior.coeff->mean,
                    prior.coeff->covariance, prior.cov->scale(h), prior.cov->df());

            post_beta.slice(h) = beta;
            post_comp_cov.slice(h) = cov;

        } else {

            /* If there is not any data in the component, which should be rare, we draw from the prior. */ 
            post_beta.slice(h) = prior.coeff->draw(false); 
            post_comp_cov.slice(h) = prior.cov->cond_draw(h); 
        }

    } 

    aw::claim_same(post_beta.n_slices, post_comp_cov.n_slices, cluster_id.max() + 1);
    aw::assert_is_finite(post_beta, post_comp_cov);
        
    return std::make_tuple(post_beta, post_comp_cov);  

}


/*
 * This function draw the transition matrix using the two dimensional dirichlet representation and the Dirichlet
 * update. 
 */
dmat draw_transition_matrix(const iuvec& cluster_id, double expected_cluster_dim) {

    /* The mean probability is (beta / (alpha + beta))^{k-1} by the mirror-symmetry and independence. */

    size_t num_comps = cluster_id.max() + 1;
    arma::umat counts(num_comps, num_comps, arma::fill::zeros);
    dmat trans_mat(num_comps, num_comps, arma::fill::zeros);

    for(size_t t= 1; t<cluster_id.size(); ++t) {

        counts(cluster_id(t-1), cluster_id(t)) += 1;

   }

    auto prior_log_prob = [=] (size_t i) {
        return i * std::log(expected_cluster_dim) - (i+1) * std::log(1 + expected_cluster_dim);
    }; 

    for(size_t k=0; k<num_comps; ++k) {
        for(size_t j=0; j<num_comps; ++j) {

          trans_mat(k,j) = std::exp(std::log(std::exp(prior_log_prob(k) + prior_log_prob(j)) + counts(k,j)) -
                  std::log(std::exp(prior_log_prob(k)) + arma::sum(counts.row(k)))); 

        }

    }


    return trans_mat;
}

/*
 * Collects the unique values in the argument maintaining the original order.
 * Note, there will not be any spaces in the new ordering, even if there were some in the old one.
 */
inline 
std::unordered_map<npulong, npulong> compute_new_index_order(const iuvec& cluster_id) {

    std::unordered_map<npulong, npulong> idx_map;
    size_t k=0;

    for(const auto idx : cluster_id)  {

        if(idx_map.count(idx) == 0) {

           idx_map.emplace(idx, k);
           ++k;
        }

    }

    return idx_map;

}



/*
 * This class actually contains the Dirichlet process itself.  The operator() provides a draw. 
 */
class cdpm { 

    private:
        /* The cluster identity parameters */
        iuvec cluster_id;
        dvec sticks;
        dmat trans_mat;

        /* The component paramters */
        dcube beta;
        dcube comp_cov;

        /* The prior */
        cdpm_prior prior;

        /* The data */
        dmat regressor;
        dmat regressand;
       
        /* Variables that govern the sampler */ 
        npulong iteration;


    public: 

    /*
     * Reorderes the indices so that they ascend with time. 
     */
    void reorder_indices() {

        auto idx_map = compute_new_index_order(cluster_id); 

        for(auto&& val : cluster_id)  {

            val = idx_map.at(val);

        }

        /* 
         * We take the betas at the old index and swap them with the betas at the new index. 
         */
        for(const auto& it : idx_map)  {

            /* We do not want to swap values back. */
            if (it.first > it.second) {

                beta.slice(it.first).swap(beta.slice(it.second));
                comp_cov.slice(it.first).swap(comp_cov.slice(it.second));
                std::swap(sticks(it.first), sticks(it.second));
                trans_mat.swap_rows(it.first, it.second);
                trans_mat.swap_cols(it.first, it.second);
                prior.cov->swap_clusters(it.first, it.second); 
            }
        }

        size_t new_cluster_dim = cluster_id.max() + 1;
        if(new_cluster_dim <= beta.n_slices) {

            prior.cov->increase_cluster_dim(new_cluster_dim);
            prior.cov->reduce_cluster_dim(new_cluster_dim);
            
            beta.resize(beta.n_rows, beta.n_cols, new_cluster_dim);
            comp_cov.resize(comp_cov.n_rows, comp_cov.n_cols, new_cluster_dim);
            trans_mat.resize(new_cluster_dim, new_cluster_dim);
            sticks.resize(new_cluster_dim);

        }

        if (sticks.n_elem >= this->time_dim())  {

            throw std::runtime_error("We have more clusters ("s + std::to_string(sticks.n_elem) 
                    + ") than we have time periods ("s + std::to_string(this->time_dim()) + ")."s);
        }

        check_valid_model(*this);
    }


    /* 
     * This function creates a valid cdpm model using the given values. It also checks to ensure that the various
     * elements are compatible.
     */
    cdpm(iuvec cluster_id, dcube beta, dcube comp_cov, dmat regressor, dmat regressand, vec_nested_map prior_)  :
        cluster_id(cluster_id), beta(beta), comp_cov(comp_cov), regressor(regressor), regressand(regressand) {

        npulong num_comps_from_cluster_id = cluster_id.max() + 1;
         
        if(num_comps_from_cluster_id > beta.n_slices) {
            throw std::invalid_argument("There number of active components as defined by cluster_id ("s +
                    std::to_string(num_comps_from_cluster_id) + ") is larger than the number from the "
                    "regressionion coefficients("s + std::to_string(beta.n_slices) +  ")."s);
        }

        if (num_comps_from_cluster_id > comp_cov.n_slices) {
            throw std::invalid_argument("There number of active components as defined by cluster_id ("s +
                    std::to_string(num_comps_from_cluster_id) + ") is larger than the number from the "s + 
                    "regressionion covariances("s + std::to_string(comp_cov.n_slices) +  ")."s);
        }
       

        this->beta.resize(regressor.n_cols, regressand.n_cols, num_comps_from_cluster_id);
        this->comp_cov = comp_cov.head_slices(num_comps_from_cluster_id);

        this->prior = cdpm_prior(prior_);
        if ((prior.coeff->mean.n_cols != beta.n_cols) || (prior.coeff->mean.n_rows != beta.n_rows)) {
                throw std::invalid_argument("The prior for the mean is specified incorrectly. It has "s
                        + std::to_string(prior.coeff->mean.n_rows) +  " rows and "s  
                        + std::to_string(prior.coeff->mean.n_cols) 
                        + " columns. It should have "s + std::to_string(beta.n_rows) + " rows and "s 
                        + std::to_string(beta.n_cols) +  " columns."s);
        }

        if ((prior.cov->scale(0).n_cols != comp_cov.n_cols) || (prior.cov->scale(0).n_rows != comp_cov.n_cols)) {
                throw std::invalid_argument("The prior for the covariance is specified incorrectly. It has "s
                        + std::to_string(prior.cov->scale(0).n_rows) +  " rows and "s  
                        + std::to_string(prior.cov->scale(0).n_cols) + " columns. It should have "s 
                        + std::to_string(comp_cov.n_cols) + " rows and "s 
                        + std::to_string(comp_cov.n_cols) +  " columns."s);
        }

        /* I draw the sticks from their posterior */
        this->sticks = draw_sticks(cluster_id, prior.get_expected_cluster_dim()); 
        this->trans_mat = draw_transition_matrix(cluster_id, prior.get_expected_cluster_dim());

        this->iteration=0;  

        check_valid_model(*this);

    }

    cdpm(const dmat& beta, dmat comp_cov, dmat regressor, dmat regressand, const vec_nested_map& prior) :
        cdpm(arma::zeros<iuvec>(regressor.n_rows), met::Cube(beta), met::Cube(comp_cov), regressor, regressand,
                prior) {}  


    cdpm(const iuvec& cluster_id_, const dmat& regressor_, const dmat& regressand_, const vec_nested_map&
            prior_) : cluster_id(cluster_id_.size()), regressor(regressor_), regressand(regressand_) {

        /* check to see if the arguments define a valid model. */ 
        aw::claim_same_n_rows(regressor_, regressand_, cluster_id_); 

        /* 
         * I need to transform the cluster_id vector that I am given into a valid one.
         * This ensures, that the first element is 0, and there are no missing components 
         */
        auto idx_map = compute_new_index_order(cluster_id_); 
        std::transform(cbegin(cluster_id_), cend(cluster_id_), begin(cluster_id),  
                       [&idx_map] (auto val) {return idx_map.at(val);});

        this->prior = cdpm_prior(prior_);
        /* I initialize the component parameters by drawing them from the posterior. */
        std::tie(this->beta, this->comp_cov) = draw_component_specific_params(cluster_id, regressor, regressand,
                prior); 

        this->sticks = draw_sticks(cluster_id, prior.get_expected_cluster_dim()); 
        this->trans_mat = draw_transition_matrix(cluster_id, prior.get_expected_cluster_dim());
        this->iteration = 0;
        check_valid_model(*this);
        
    }  

    /*
     * Computes the parameters for a new component by drawing the unkown ones from their priors until their are at
     * least new_cluster_dim clusters of component paramters
     */ 
    void add_new_component_params(npulong new_cluster_dim) {

        npulong old_cluster_dim = cluster_dim(); 

        while (new_cluster_dim > old_cluster_dim) {
    
            if(new_cluster_dim > beta.n_slices) {
                aw::append_inplace(this->beta, prior.coeff->draw(false)); 
            }

            if(new_cluster_dim > comp_cov.n_slices) {
                aw::append_inplace(this->comp_cov, prior.cov->cond_draw(new_cluster_dim));          
            }
            old_cluster_dim++; 
        }

        this->prior.cov->increase_cluster_dim(new_cluster_dim); 
    
    }
    
    
    /*
     * Takes the time and cluster identity and computes the implied coefficient weight. 
     */
    double coeff_weight(size_t t , size_t k) {

        dvec realization = arma::vectorise(regressand.row(t));
    
        if (k >= comp_cov.n_slices) {
            this->add_new_component_params(k+1u); 
        }

        /* cluster_dim() has changed the value from above. */
        aw::claim_same(cluster_dim(), beta.n_slices, comp_cov.n_slices);
        assert(k < cluster_dim());

        dmat var = comp_cov.slice(k);
        dvec mean = arma::vectorise(regressor.row(t) * beta.slice(k));

        return std::exp(multivariate_normal_log_density(realization, mean, var)); 

    }

    inline
    drow forecast_regressand() const { 

        npulong cluster_id_val = cluster_id(cluster_id.n_rows - 1); 
        
        drow forecast_row = regressor.tail_rows(1) * beta.slice(cluster_id_val) +
            lt_root_mat(comp_cov.slice(cluster_id_val)) * randn(regressand_dim()).t(); 

        return forecast_row;

    }    

    const dmat& get_regressor() const { return regressor;}
    dmat& set_regressor() {return regressor;}
    void set_regressor(const dmat& reg) {
        if (! aw::check_same_size(reg, regressor)) {
            throw std::invalid_argument("The regressor cannot change size.");
        }
        if (! aw::check_is_finite(reg)) {

            throw std::invalid_argument("The regressor must be finite and non-nan.");
        }
        regressor = reg;
    }

    const dmat& get_regressand() const {return regressand;}
    dcube get_cov_means() const {return prior.cov->scales();}

    dmat& set_regressand() {return regressand;}

    void set_regressand(const dmat& reg) {
        if (! aw::check_same_size(reg, regressand)) {
            throw std::invalid_argument("The regressand cannot change size.");
        }
        if (! aw::check_is_finite(reg)) {

            throw std::invalid_argument("The regressand must be finite and non-nan.");
        }
        regressand = reg;
    }

    npulong regressand_dim() const {return regressand.n_cols;}
    npulong time_dim() const { return regressor.n_rows;}
    npulong regressor_dim() const {return regressor.n_cols;} 
    npulong cluster_dim() const {return beta.n_slices;}

    const iuvec& get_cluster_id() const {return cluster_id;}

    /* 
     * This function changes cluster_id to the given value. It also changes the number of clusters to maintain
     * validity of the model. If the number of clusters grows, it fills in the new value with the mean previous
     * values.
     */
    void set_cluster_id(const iuvec& new_cluster_id) {

        if (! aw::check_same_size(new_cluster_id, cluster_id)) {
            throw std::invalid_argument("The indices vector cannot change its size.");
        }
        
        npulong new_num_comps = new_cluster_id.max() + 1;
        npulong old_cluster_dim = cluster_dim();

        // We eliminate unnecessary clusters.
        if (new_num_comps < cluster_dim()) {

            this->beta.resize(beta.n_rows, beta.n_cols, new_num_comps);
            this->comp_cov.resize(comp_cov.n_rows, comp_cov.n_cols, new_num_comps);
            this->sticks.resize(new_num_comps);
            this->trans_mat.resize(new_num_comps, new_num_comps);
        }
        // We add more clusters, by drawing the new paramters from the prior.
        if (new_num_comps > cluster_dim()) {

            auto old_beta = beta;
            auto old_comp_cov = comp_cov;

            std::tie(this->beta, this->comp_cov) = draw_component_specific_params(cluster_id, regressor,
                    regressand, prior); 
            this->beta.head_slices(old_cluster_dim) = old_beta;
            this->comp_cov.head_slices(old_cluster_dim) = old_comp_cov;

            dmat old_trans_mat = this->trans_mat;
            this->trans_mat = draw_transition_matrix(new_cluster_id, prior.get_expected_cluster_dim());
            this->trans_mat.submat(0, 0, old_cluster_dim-1, old_cluster_dim-1) = old_trans_mat;

            auto old_sticks = sticks;
            this->sticks = draw_sticks(cluster_id, prior.get_expected_cluster_dim()); 
            sticks.head_rows(old_cluster_dim) = old_sticks; 
        }

        cluster_id = new_cluster_id;
        reorder_indices();
        check_valid_model(*this);
    }  
    
    const dcube& get_beta() const {return beta;}
    dcube& set_beta() {return beta;}

    const dcube& get_comp_cov() const {return comp_cov;}
    dcube& set_comp_cov() {return comp_cov;} 

    const dmat& get_trans_mat() const {return trans_mat;}
    dmat& set_trans_mat() {return trans_mat;} 

    const dvec& get_sticks() const {return sticks;}
    dvec& set_sticks() { return sticks;}

    dvec get_probabilities() const {
        return compute_probabilities(sticks);

    }


    std::tuple<iuvec, dvec, dcube, dmat, dcube, dcube, dmat> operator() (bool draw_parameters=true, bool
            draw_indices=true) {

        check_valid_model(*this);

        if (draw_parameters) {

            /* Draw the component specific parameters */ 
            std::tie(beta, comp_cov) = draw_component_specific_params(cluster_id, regressor, regressand, prior); 

            /* Update the prior for the coefficients and covariances.*/
            prior.cov->fit(comp_cov);
            prior.coeff->fit(beta, comp_cov);
        }


        if (draw_indices) {
            
            /* First I draw the sticks from the marginal (w.r.t U) using Papaspilopoulos & Roberts (2008) */
            sticks = draw_sticks(cluster_id, prior.get_expected_cluster_dim()); 

            /* Then I draw the allocations again using Walker (2007) */
            auto coeff_weight_lambda = [&] (auto t, auto k) {return this->coeff_weight(t,k);};
            
            /* I have to keep the sizes of cluster_id, sticks, beta, and cov consistent.' */
            std::tie(cluster_id, sticks) = draw_cluster_id(cluster_id, sticks, trans_mat, coeff_weight_lambda,
                    prior.get_expected_cluster_dim());

            /* If I have to many cluster components, I drop the unnecessary parameters. */
            if (sticks.size() < cluster_dim()) {
               beta.resize(beta.n_rows, beta.n_cols, sticks.size()); 
               comp_cov.resize(comp_cov.n_rows, comp_cov.n_cols, sticks.size()); 

            } else if (sticks.size() > cluster_dim()) {
                assert(false && "The stick dimension cannot be larger than the cluster dimension.");
            }

            this->trans_mat = draw_transition_matrix(cluster_id, prior.get_expected_cluster_dim());
        }

        assert(trans_mat.n_rows == cluster_dim());
        reorder_indices();

        /* Keep track of which iteration we are on. */
        ++iteration; 
        check_valid_model(*this);

       return std::make_tuple(cluster_id, sticks, beta, prior.coeff->mean,
               comp_cov, prior.cov->scales(), trans_mat);
    
    } 

};






/* 
 * I copy the the regressor data._dynamics_model.pi and append the new data point to he last row.
 * I further assume that the  regresosr has rows of the form y_t, y_{t-1}.... Consequently, the new 
 * last row is simply the the old last row shifted over one, with the last element dropped off.
 * This assumes that the newest data is at the botttom of the matrices.  
 */       
//TODO Check this!
inline void forecast_regressor(cdpm& model) { 

    auto old_time_dim = model.time_dim(); 
    const auto& regressor = model.get_regressor();  
    
    drow new_regressor_row(model.regressor_dim());    
    /* VAR(1) Model */
    if (model.regressand_dim() == model.regressor_dim() ) {

        new_regressor_row = model.get_regressand().tail_rows(1); 

    /* VAR(N) case */
    } else if (model.regressand_dim() < model.regressor_dim()) {
        arma::span regressor_cols(0, model.regressor_dim() - (model.regressand_dim() + 1));     
        new_regressor_row = arma::join_horiz(model.get_regressand().tail_rows(1),
                                                  regressor(old_time_dim -1, regressor_cols));

    } else {

        throw std::invalid_argument("The regressand dimension (" + std::to_string(model.regressand_dim()) 
                + ") must be less than or equal to the regressor dimension ("s 
                + std::to_string(model.regressor_dim()) + ")."s);
    
    } 

    aw::append_inplace(model.set_regressor(), new_regressor_row);

}


/* Forecasts the regressand forward one period. */
//TODO Check this!
inline void forecast_regressand(cdpm& model) {

    drow forecast_row = model.forecast_regressand(); 
    aw::append_inplace(model.set_regressand(), forecast_row);  


}
/*
 * This function simply asserts that the model is consistent. 
 */ 
void check_valid_model(const cdpm& model) {

    bool consistent_time_dim = aw::check_same_n_rows(model.get_regressor(), model.get_regressand(),
                                                     model.get_cluster_id());

    if(! consistent_time_dim) {

        throw std::invalid_argument("The model has parameters with conflicing time dimensions."); 

    }

    bool consistent_regressand_dim = aw::check_same(model.get_beta().n_cols, model.get_regressand().n_cols, 
                                                    model.get_comp_cov().n_cols, model.get_comp_cov().n_rows,
                                                    model.get_cov_means().n_cols, model.get_cov_means().n_rows); 


    if (! consistent_regressand_dim) { 
        
        throw std::invalid_argument("The model has parameters with conflicting regressand dimensions.");

    }

    bool consistent_regressor_dim = (model.get_beta().n_rows == model.get_regressor().n_cols);
    if (! consistent_regressor_dim) {

        throw std::invalid_argument("The model has parameters with conflicting regressor dimensions.");
    }

    bool consistent_cluster_dim = aw::check_same(model.get_beta().n_slices, model.get_comp_cov().n_slices,
                                                 model.get_sticks().size(), model.get_cluster_id().max() + 1,
                                                 model.get_trans_mat().n_rows, model.get_trans_mat().n_cols);

    if (! consistent_cluster_dim ) {
    
        throw std::invalid_argument("The model has parameters with a conflicting number of clusters. "s 
                + "The coefficient  has "s + std::to_string(model.get_beta().n_slices) 
                +  " slices. The covariance has "s
                + std::to_string(model.get_comp_cov().n_slices) + " The trans_mat has "s
                + std::to_string(model.get_trans_mat().n_cols) + " columns and "s
                + std::to_string(model.get_trans_mat().n_rows) + " rows. The sticks take "s
                + std::to_string(model.get_sticks().size()) + " values. The cluster idenities take "s 
                + std::to_string(model.get_cluster_id().max() + 1) + " values."s);

    }

    bool consistent_mean_cluster_dim = (model.get_cov_means().n_slices == 1) 
        || (model.get_cov_means().n_slices == model.get_beta().n_slices);

    if (! consistent_mean_cluster_dim) {
    
        throw std::invalid_argument("The model has parameters with a conflicting number of clusters."s 
                " The model's cluster dimension is "s + std::to_string(model.cluster_dim()) + 
                "the means have "s + std::to_string(model.get_cov_means().n_slices) + " clusters."s);

    }
    
    bool is_finite = aw::check_is_finite(model.get_regressor(), model.get_regressand(), model.get_cluster_id(),
                                         model.get_beta(), model.get_comp_cov(), model.get_trans_mat()); 

   if (! is_finite) {

        throw std::domain_error("At least one of the parameters in the model is not filled entirely with "
                                "finite numbers.");
    }  

   if (! arma::all(model.get_sticks() >= 0)) {
        throw std::domain_error("The sticks must all be nonnegative.");
   }

    for (size_t k=0; k<model.cluster_dim(); ++k) {
        double row_sum = arma::sum(model.get_trans_mat().row(k)); 
        if (row_sum > 1.0 + 1e-8) {
            throw std::runtime_error("Row "s + std::to_string(k)  + " of the transition matrix sums to "s 
                    + std::to_string(arma::sum(model.get_trans_mat().row(k))) + " which is greater than 1.0."s);
        }
    }

}


/*
 * Provides a forecast from the model. num_comps is the maximum allowed number of components.
 * If it is negative, no maximum is imposed. 
 */
cdpm forecast(cdpm model, npulong forecast_dim) { 

    assert(false && "This is not implemented correctly. It does not forecast the cluster_id's");
    if (forecast_dim < 1) {

        throw std::invalid_argument("The forecast dimension ("s + std::to_string(forecast_dim) 
                + ") must be at least 1."s);
    }

    for(npulong t=0; t<forecast_dim; ++t) {

        forecast_regressor(model); 
        forecast_regressand(model);   
    }

    return model;
 
}



/*
 * This method performs one-step ahead forecasts. 
 */
inline
dmat predict_cdpm(const iuvec& cluster_id, const dmat& trans_mat, const dcube& beta, const dcube& comp_cov,
                       const dmat& regressor) {
    
    /*
     * Check that the model is valid. 
     */
    {
        if ((trans_mat.n_cols < cluster_id.max()) || (trans_mat.n_cols != trans_mat.n_rows)) {
            throw std::invalid_argument("The given cluster identity vector and transision matrix are"s 
                                        + " incompatible."s);
        }

        if (beta.n_rows != regressor.n_cols) {
            throw std::invalid_argument("The beta coefficient and regressor have conflicting regressor "s 
                                        + "dimensions: ("s + std::to_string(beta.n_rows) + ","s 
                                        + std::to_string(regressor.n_cols) + ")."s);
        }

        if ((comp_cov.n_rows != comp_cov.n_cols) || comp_cov.n_cols != beta.n_cols)  { 
            throw std::invalid_argument("The beta coefficient and error covariance have conflicting "s 
                    "regressand dimension."s);
        }

        if (cluster_id.n_rows != regressor.n_rows) {
            throw std::invalid_argument("The cluster_id has "s + std::to_string(cluster_id.n_rows) 
                    + " rows, and the regressor has " + std::to_string(regressor.n_rows) 
                    + " which  are incompatible."s);
        }
    }


    size_t time_dim = regressor.n_rows;
    size_t regressand_dim = comp_cov.n_cols;
    dmat forecast(time_dim, regressand_dim);

    for(size_t t=0; t<time_dim; ++t) {

        npulong cluster_id_val = cluster_id(t);
        dvec weights = trans_mat.row(cluster_id_val).t();
        weights /=  arma::accu(weights);

        npulong next_cluster_id = std::discrete_distribution<npulong>(cbegin(weights),
                cend(weights))(initialize_mt_generator());
        dmat root_cov = lt_root_mat(comp_cov.slice(next_cluster_id));
        dmat coeff = beta.slice(next_cluster_id); 

        dmat tmp_mat = regressor.row(t) * coeff + (root_cov * met::randn(regressand_dim)).t();
        
        forecast.row(t) = tmp_mat;

    }
   

    return forecast;


}


// This can be parallelized, because I am only forecast one period forward.  
inline dmat predict_ols(const dmat& regressor, const dmat& beta, const dmat& cov) {

    npulong time_dim = regressor.n_rows;
    npulong regressand_dim = cov.n_cols;

    dmat forecast(time_dim, regressand_dim);
    dmat root_cov = lt_root_mat(cov);

    for(size_t t=0; t<time_dim; ++t) {

        dmat tmp_mat = regressor.row(t) * beta + (root_cov * met::randn(regressand_dim)).t();
        forecast.row(t) = tmp_mat;

    }

    return forecast;

}



/*
 *  This function draws the component parameters assuming that there is only one component, i.e. it estimates a
 *  Bayesian VAR
 */ 
inline
std::tuple<dmat, dmat> draw_OLS_params(const dmat& regressor, const dmat& regressand, cdpm_prior prior) 
{

    if (regressor.n_rows != regressand.n_rows) {
        throw std::invalid_argument("The regressor (n_rows: "s + std::to_string(regressor.n_rows) 
                + ") and regressand (n_rows:"s + std::to_string(regressand.n_rows) + 
                "must have the same number of rows."s);
    }

    if (! arma::is_finite(regressor)) {
        throw std::invalid_argument("The regressor must be filled with real (finite) numbers.");
    }

    if (! arma::is_finite(regressand)) {
        throw std::invalid_argument("The regressand must be filled with real (finite) numbers.");
    }

    /*
     * In this prior we only consider the prior for the covariance with one component. 
     */
    auto [post_beta, post_comp_cov]  = draw_coeff_and_cov(regressor, regressand, prior.coeff->mean, 
            prior.coeff->covariance, prior.cov->scale(0), prior.cov->df()); 
   
    aw::assert_is_finite(post_beta, post_comp_cov);
        
    return std::make_tuple(post_beta, post_comp_cov);  

}

/* End of namespace cdpm */
}



