#ifndef METROP_H
#define METROP_H

#include <cmath>
#include <random>
#include <vector>
#include <armadillo>
#include <arma_wrapper.h>


namespace met {

using namespace std::string_literals;
using std::end;
using std::begin;
using std::cend;
using std::cbegin;
using aw::dmat;
using aw::dcube;
using aw::npdouble;


static npdouble pi = arma::datum::pi; 
static npdouble log2pi = std::log(2 * arma::datum::pi); 

template<typename ArithmeticType>
arma::Cube<ArithmeticType> Cube(const arma::Mat<ArithmeticType>& mat) {
    
    arma::Cube<ArithmeticType> returncube(mat.memptr(), mat.n_rows, mat.n_cols, 1);

    return returncube;


}

/* Computes the sum of squares */
aw::npdouble sum_of_squares(const dmat& arr) { 

    return arma::accu(arma::pow(arr, 2)); 
}

/*
 * This function simply averages the matrix with its tranpose.
 */
inline
dmat make_symmetric(const dmat &matrix) {
    return .5 * (matrix + matrix.t());
}


/*
 * This function simply averages the matrix with its tranpose.
 */
inline
dmat make_symmetric(const dmat &matrix1, const dmat matrix2) {
    return .5 * (matrix1 + matrix2.t());
}

/*
 * Calculates the lower triangular root of a matrix.  If the matrix is positive-definite. it uses the cholesky
 * decomposition since that method is very fast and numerically stable. However, it uses the SVD decomposition 
 * to handle rank-deficient matrices.  To get a triangular root from the SVD decomposition, you need to use the 
 * QR factorization after the SVD decomposition, and so that is done as well. 
 */
inline dmat lt_root_mat(const dmat& psd_mat) {

    dmat root_mat(arma::size(psd_mat));
    bool factored_mat = arma::chol(root_mat, psd_mat, "lower");

    if (!factored_mat) {
        dmat u_mat(arma::size(psd_mat));
        dmat v_mat(arma::size(psd_mat));
        aw::dvec singular_vals(psd_mat.n_rows);
        dmat q_mat(arma::size(psd_mat));

        factored_mat = arma::svd(u_mat, singular_vals, v_mat, make_symmetric(psd_mat));
        if (!factored_mat) {
            throw std::runtime_error("The matrix could not be SVD decomposed.");
         }

        dmat right_root = arma::diagmat(arma::sqrt(singular_vals)) * make_symmetric(u_mat, v_mat);
        factored_mat = arma::qr(q_mat, root_mat, right_root);

        /* We want the entries on the diagonal to be positive as the are in the Cholesky decomposition. */
        if (root_mat(0) < 0) {
            root_mat = -1 * root_mat;
        }

        if (!factored_mat) {
            throw std::runtime_error("We could not QR decompose the matrix.");
        }

        /* We want the left root. */ 
        arma::inplace_trans(root_mat); 
     }

    return arma::trimatl(root_mat);
}

/*
 * This function solves takes the lower cholesky factor of a matrix (A) and solves the system A \ B. 
 * It is inspired by to the cho_solve function from scipy.
 */
inline
dmat cho_solve(const dmat &lroot, const dmat &mat2) {
    return arma::solve(arma::trimatu(lroot.t()), arma::solve(arma::trimatl(lroot), mat2));
}

template<typename T1, typename T2>
npdouble multivariate_normal_log_density(const arma::Col<T1> &x, const T2 &mean, const arma::Mat<T1> &covariance) {

    size_t num_obs = x.size();
    dmat root_cov = arma::trimatl(lt_root_mat(covariance));
    double log_det = arma::accu(arma::log(root_cov.diag())); 

    npdouble log_density = -.5 * num_obs * log2pi + log_det; 
    
    log_density -= .5 * arma::dot((x - mean), cho_solve(root_cov, (x - mean))); 
    
    return log_density;

}

/*  Compute the gaussian log_pdf.  */
template<typename RealType> 
typename std::enable_if_t<std::is_floating_point<RealType>::value, RealType> 
normal_log_pdf(const RealType x, const RealType mean=0, const RealType stddev=1) { 

    static RealType propr_constant = .5 * std::log(2 * arma::datum::pi);
    RealType root_kernel = (x - mean ) / stddev; 

    return -1 * (propr_constant +  std::log(stddev) + .5 * std::pow(root_kernel,2));
     
}

template <typename RealType>
auto normal_pdf(const RealType x, const RealType mean=0, const RealType stddev=1) {
    return std::exp(normal_log_pdf(x, mean, stddev)); 

}

template <typename RealType>
auto normal_cdf(const RealType x) { 

    RealType inv_sqrt_2 = 1 / std::sqrt(2); 
    return .5 * (1 + std::erf(x * inv_sqrt_2)); 

}

inline
aw::npdouble exp_log_pdf(aw::npdouble lambda, aw::npdouble x) { 

       return std::log(lambda) - lambda * x; 

}

inline
auto log_student_t_pdf(aw::npdouble x, aw::npulong nu, aw::npdouble mean=0, aw::npdouble scale=1) { 


    auto rescaled_val = (x - mean) / scale;
    auto prop_val = std::lgamma(.5 * (nu + 1)) - std::log(std::sqrt(nu * pi)) - std::lgamma(.5 * nu);
    auto log_kernel = (-.5 * (nu + 1)) * std::log1p(rescaled_val * rescaled_val / nu); 

   return prop_val + log_kernel;  

}

/* 
 * Initializes a random generator in a thread_save way to be used globally througout the entire library. 
 */
inline
std::mt19937_64& initialize_mt_generator() { 
    
    thread_local std::random_device rd; 
    thread_local std::mt19937_64 engine(rd());
    
    return engine; 

}

/* Draws a standard normal random variable using the std library's distribution and the given generator. */
inline
aw::dvec randn(const aw::npulong n_elem) {
    
    thread_local std::normal_distribution<aw::npdouble> n_dist(0,1);        
    aw::dvec variates(n_elem);
    thread_local auto& generator = initialize_mt_generator();  
    for(auto&& val : variates) {
    
            val = n_dist(generator);

    }

    return variates; 
}


template<typename Generator> 
dmat draw_root_precision(Generator& g, const dmat& precision_factor, double degrees_of_freedom) {

    size_t dim = precision_factor.n_rows;
    dmat A(dim, dim, arma::fill::zeros);
    std::normal_distribution<double> n_dist;
    
    /* 
     * I am using the Bartlett decomposition, and filling the off-diagonal elements of A with N(0,1)
     */
    for(size_t i=0; i<dim; ++i) {

       /* We add 2 instead of 1 becasue we start counting at 0. */
       std::chi_squared_distribution<double> chi_dist(degrees_of_freedom - i);
       A(i,i) = std::sqrt(chi_dist(g)); 

        for(aw::npulong j=0; j<i; ++j) {
            A(i,j) = n_dist(g); 
        }
    }

    dmat root_precision = precision_factor * A;

    return root_precision;
} 


/*
 * This class defines an Inverse-Wishart distribution.
 * It uses the root_precision to solve for the inverse.
 */
class inverse_wishart_distribution {

    private:
        dmat precision_factor;
        const double degrees_of_freedom;

    public:
        inverse_wishart_distribution(double degrees_of_freedom_, dmat scale) :
            degrees_of_freedom{degrees_of_freedom_} {

            dmat identity = arma::eye<dmat>(scale.n_rows,scale.n_rows);
            precision_factor = arma::solve(lt_root_mat(scale), identity); 
            
            if(degrees_of_freedom <= dim() - 1) {
                throw std::invalid_argument("The degrees of freedom ("s + std::to_string(degrees_of_freedom) +
                            ") must be greater than the dimension("s + std::to_string(scale.n_rows) + ") - 1."s);
            }
        }

    size_t dim() const { return precision_factor.n_rows;}

    template<typename Generator> 
    dmat operator()(Generator& g) const {

        dmat root_precision = draw_root_precision(g, precision_factor, degrees_of_freedom);
        dmat return_mat =  cho_solve(root_precision, arma::eye(dim(), dim()));

        return return_mat;

    }

};

/*
 * This class defines a Wishart distriobution.
 * Instead of using the root precision to solve for the inverse, it multplies the root precision with itself.
 */
class wishart_distribution {

    private:
        dmat precision_factor;
        const double degrees_of_freedom;

    public:
    wishart_distribution(double degrees_of_freedom_, dmat scale) :
        degrees_of_freedom{degrees_of_freedom_} {

        precision_factor = lt_root_mat(scale); 
        
        if(degrees_of_freedom <= dim() - 1) {
            throw std::invalid_argument("The degrees of freedom ("s + std::to_string(degrees_of_freedom) +
                        ") must be greater than the dimension("s + std::to_string(scale.n_rows) + ") - 1."s);
        }
    }

    size_t dim() const { return precision_factor.n_rows;}

    template<typename Generator> 
    dmat operator()(Generator& g) const {

        dmat root_precision = draw_root_precision(g, precision_factor, degrees_of_freedom);
        dmat returnmat = root_precision * root_precision.t(); 

        return returnmat;

    }

};

class inverse_gamma_distribution {

    private:
        /* 
         * We use the matrix generalization to do all of the work. 
         */
        const inverse_wishart_distribution mat_dist;

    public:

        /*
         * You can only convert positive scalars into positive definite matrices.
         */
        inline static dmat check_psd(double arg) {

            if(arg <= 0) {
                throw std::invalid_argument("Scale parameter ("s + std::to_string(arg) + ") must be postive"s);
            }
        
            return dmat({arg});

        }

        inverse_gamma_distribution(double degrees_of_freedom, double scale) :
            mat_dist{2 * degrees_of_freedom, 2 * check_psd(scale)} {} 

        template<typename Generator> 
        double operator()(Generator& g) const {

            return arma::as_scalar(mat_dist(g)); 

        }

};


/*
 * Draw a beta random variable with the specified parameters. 
 */
double draw_beta(double alpha, double beta) {

    double x = std::chi_squared_distribution<double>(2 * alpha)(initialize_mt_generator());
    double y = std::chi_squared_distribution<double>(2 * beta)(initialize_mt_generator());

    return x / (x + y);
}


/* 
 * A farlly standard random walk metropolis hastings algorithm for a scalar type with a gaussian 
 * proposal distribution. 
 */
template<typename DrawType, typename ScaleType, typename FuncType> 
std::tuple<DrawType, bool>  inner_metropolis_func(const FuncType& log_posterior,const DrawType& previous_val, 
                            const ScaleType& scale, const bool force_positive=false) { 

    static_assert(std::is_arithmetic<DrawType>::value, 
                  "The current implmentation only allows you to draw scalars."); 

    thread_local auto& rng = initialize_mt_generator();
    std::normal_distribution<DrawType> n_dist(previous_val, scale);  
    std::uniform_real_distribution<> u_dist(0.0,1.0);  

    /* 
     * We do not have to adjust for the ratio of the proposal distributions because the truncated normal
     * is still symmetric in its arguments. 
     */  
    DrawType proposal = (force_positive) ? std::abs(n_dist(rng)) : n_dist(rng); 

    aw::npdouble new_post = log_posterior(proposal);   
    aw::npdouble old_post = log_posterior(previous_val);

    if ((new_post - old_post) > std::log(u_dist(rng))) { 
        return std::make_tuple(proposal, true); 

    } else { 

        return std::make_tuple(previous_val, false); 
    } 
}


/*
 * A fairly standard random walk maetrpolis hastings algorithm with a proposal that comes from a normal
 * distribution. The current implmentation assumes that that the previous vale and scale are scalars.  
 * Note, I am using scale here, i.e. the standard deviation of the propsoal distribution. We should be a
 * accepting about 1/3 of the draws. Consequently, I increase the scale by twice as much as I decrease it.  
 */
template<typename DrawType, typename ScaleType, typename FuncType> 
std::tuple<DrawType, bool, ScaleType> metropolis_func(const FuncType& log_posterior,
                                                      const DrawType& previous_val, 
                                                      const ScaleType& scale, const aw::npulong iteration, 
                                                      const aw::npulong burnin_iteration=200,
                                                      const bool force_positive=false) {

    DrawType return_val; 
    bool accepted;
  
    std::tie(return_val, accepted) = inner_metropolis_func(log_posterior, previous_val, scale, force_positive);  

    if (iteration > burnin_iteration) { 
    
            return std::make_tuple(return_val, accepted, scale);  

    } else { 
        
        if (accepted) { 
            
            return std::make_tuple(return_val, accepted, 1.10 * scale); 

        } else { 

            return std::make_tuple(return_val, accepted, .95 * scale); 

        }
    }
}

inline 
std::tuple<dmat, dmat> matrix_heteroskedastic_mean_moments(const dcube& data,
        const dcube& covariances, const dmat& prior_mean) {
    /*
     * This function implements the heteroskedastic mean estimator described in
     * the accompanying documentation assuming that $V$ is the idenity matrix.
     */

    assert(data.n_slices == covariances.n_slices); 
    aw::claim_same_n_cols(data, prior_mean, covariances);
    aw::claim_same_n_rows(data, prior_mean);
    aw::assert_square(covariances);

    dmat weighted_data = arma::zeros(arma::size(data.slice(0)));
    dmat precision_sum = arma::zeros(arma::size(covariances.slice(0)));

    for (size_t i = 0; i < data.n_slices; ++i) {

        dmat precision_mat = arma::pinv(covariances.slice(i));
        weighted_data += data.slice(i) * precision_mat; 
        precision_sum += precision_mat;
    }

    dmat unweighted_post_mean = prior_mean + weighted_data; 
    dmat trans_precision_factored =
        lt_root_mat(arma::eye(arma::size(precision_sum)) + precision_sum); 
    
    dmat mean = cho_solve(trans_precision_factored, unweighted_post_mean.t()).t();

    return std::make_tuple(trans_precision_factored, mean);


}

inline
dmat draw_hetero_innov_cov(const dcube& data, const dcube& covariances, const
        dmat& prior_mean, const dmat& prior_scale, double prior_df) {

    /* Specify the preconditions */
    aw::assert_is_finite(data, covariances, prior_mean, prior_scale); 
    assert(prior_df >= 0);

    assert(data.n_slices == covariances.n_slices); 
    aw::claim_same_n_cols(data, prior_mean, covariances);
    aw::claim_same_n_rows(data, prior_mean, prior_scale);
    aw::assert_square(covariances, prior_scale);
        
    size_t cluster_dim = data.n_slices;
    size_t regressand_dim = data.n_cols;

    dmat weighted_squared_data = arma::zeros(arma::size(prior_scale));
    dmat left_weighted_squared_data = arma::zeros(data.n_rows, data.n_cols);
    dmat precision_sum = arma::zeros(arma::size(covariances.slice(0)));

    for (size_t i = 0; i < cluster_dim; ++i) {

        dmat precision = arma::pinv(covariances.slice(i)); 
        left_weighted_squared_data += data.slice(i) * precision;
        weighted_squared_data += data.slice(i) * precision * data.slice(i).t(); 
        precision_sum += precision; 
    }

    /* 
     * I am assumming that $V$ is the identity matrix.
     */
    dmat weighted_mean = prior_mean + left_weighted_squared_data;
    dmat weighted_mean_squared = weighted_mean * arma::solve(precision_sum
            + arma::eye(arma::size(precision_sum)), weighted_mean.t());

    /*
     * The post_scale is of the form: Sum(X 'X) - Sum(E[X]' E[X])
     */
    dmat post_scale = prior_mean * prior_mean.t() +  weighted_squared_data +
        prior_scale - weighted_mean_squared;

    double post_df = prior_df + (cluster_dim + 1) * regressand_dim; 

    inverse_wishart_distribution g_dist(post_df, post_scale); 
    dmat draw  = g_dist(initialize_mt_generator()); 

    return draw;

}


inline std::tuple<dmat, dmat> matrix_coeff_moments(const dmat& regressor, const
        dmat& regressand, const dmat& prior_mean, const dmat& prior_col_cov) {

    const auto data_dim = regressand.n_cols; 
    const auto state_dim = regressor.n_cols; 

    aw::claim_same_n_rows(regressor, regressand);
    aw::claim_same_n_cols(regressand, prior_mean);
    aw::claim_same_n_cols(regressor, prior_col_cov);
    aw::assert_square(prior_col_cov);
    assert(regressor.n_cols == prior_mean.n_rows);

    dmat inverse_prior_col_cov = cho_solve(lt_root_mat(prior_col_cov), arma::eye(arma::size(prior_col_cov))); 
    dmat weighted_prior_coeff = inverse_prior_col_cov * prior_mean;
    dmat unweighted_post_mean = weighted_prior_coeff + regressor.t() * regressand;
    dmat trans_precision_factored = lt_root_mat(inverse_prior_col_cov +
            regressor.t() * regressor); 

    dmat mean = cho_solve(trans_precision_factored, unweighted_post_mean);
    
    return std::make_tuple(trans_precision_factored, mean);
}



/*
 * This function draws from the marginal posterior of a  inverse-wishart random
 * variable wth an associated matrix-normal coefficient using their conjugacy
 * properties. The formula is derived in the accompapaning paper.
 */
inline
dmat draw_innov_cov(const dmat& regressor, const dmat& regressand, const dmat&
        prior_coeff_mean, const dmat& prior_coeff_col_cov, const dmat&
        prior_scale, const aw::npdouble prior_df) {

    /* Specify the preconditions */
    aw::assert_is_finite(regressor, regressand); 
    assert(prior_df >= 0);
    aw::assert_is_finite(regressor, regressand, prior_coeff_mean, prior_coeff_col_cov, prior_scale); 

    aw::claim_same_n_rows(regressor, regressand);
    assert(prior_coeff_mean.n_rows == regressor.n_cols);
    aw::claim_same_n_cols(regressand, prior_coeff_mean, prior_scale);
    aw::assert_square(prior_scale, prior_coeff_col_cov);
    aw::claim_same_n_cols(regressor, prior_coeff_col_cov);
    assert(regressor.n_cols == prior_coeff_mean.n_rows);
        
    size_t data_dim = regressor.n_rows; 
    dmat inverse_prior_coeff_col_cov = cho_solve(lt_root_mat(prior_coeff_col_cov), 
            arma::eye(arma::size(prior_coeff_col_cov))); 
    dmat weighted_prior_coeff = inverse_prior_coeff_col_cov * prior_coeff_mean;
    dmat scale_part1 = prior_coeff_mean.t() * weighted_prior_coeff 
        + regressand.t() * regressand + prior_scale;

    dmat unweighted_mean  =  weighted_prior_coeff + regressor.t() * regressand;
    dmat left_root_mat = lt_root_mat(inverse_prior_coeff_col_cov + regressor.t()  * regressor);

    dmat scale_part2 = unweighted_mean.t() * cho_solve(left_root_mat, unweighted_mean);

    inverse_wishart_distribution g_dist(data_dim + prior_df, scale_part1 - scale_part2); 
    dmat draw  = g_dist(initialize_mt_generator()); 

    return draw;

}


std::tuple<dmat,dmat> draw_coeff_and_cov(const dmat& regressor, const dmat& regressand, const dmat&
        prior_coeff_mean, const dmat& prior_coeff_cov, const dmat& prior_scale, const aw::npdouble prior_df)  { 
        
    /* Specify the preconditions. */
    aw::assert_is_finite(regressor, regressand, prior_coeff_mean, prior_coeff_cov, prior_scale); 
    aw::claim_same_n_rows(regressor, regressand);
       
    aw::claim_same_n_cols(regressand, prior_coeff_mean);  
    aw::assert_square(prior_coeff_cov);
    aw::claim_same_n_cols(regressor, prior_coeff_cov);
    assert(regressor.n_cols  == prior_coeff_mean.n_rows);
          
    aw::npulong regressor_dim = regressor.n_cols; 
    aw::npulong regressand_dim = regressand.n_cols; 

    dmat innov_cov_draw = draw_innov_cov(regressor, regressand, prior_coeff_mean,
            prior_coeff_cov, prior_scale, prior_df); 

    auto [post_left_precision_factored, coeff_mean] = matrix_coeff_moments(regressor, regressand, 
            prior_coeff_mean, prior_coeff_cov); 

    dmat innovations = arma::reshape(randn(regressor_dim * regressand_dim), regressor_dim, regressand_dim);
    dmat root_innov_cov_draw = lt_root_mat(innov_cov_draw);
    dmat left_cov = arma::inv(post_left_precision_factored); 

    dmat coeff_draw = coeff_mean + left_cov * innovations * root_innov_cov_draw; 

    return  std::make_tuple(coeff_draw, innov_cov_draw);
}


/*
 * This function checks if its arguments are stationary by checking if all of its eigenvalues are in the unit
 * circle. If the matrix has more rows than columns, it assumes that the top rows correspond to the lagged
 * values. I.e. the mean was placed in front in the transpose.
 */

bool is_stationary(const dmat& matrix) {

    auto regressand_dim = matrix.n_cols;
    bool stationary = arma::all(arma::abs(arma::eig_gen(matrix.tail_rows(regressand_dim))) < 1);

    return stationary;

}



/* If you are passing the previous value of the matrix, we assume that youw want to enforce stationarity. */
std::tuple<dmat,dmat>  draw_coeff_and_cov(const dmat& regressor, const dmat& regressand, const dmat&
        prior_coeff_mean, const dmat& prior_coeff_cov, const dmat& prior_scale, const aw::npdouble prior_df, const
        dmat& default_value, bool force_stationarity=false) { 

    auto [coeff_draw, innov_cov_draw]  = draw_coeff_and_cov(regressor, regressand, prior_coeff_mean, prior_coeff_cov,
            prior_scale, prior_df); 
    
    if (coeff_draw.has_nan() || (force_stationarity && !is_stationary(coeff_draw))) {

        if( ! aw::check_same_size(default_value, coeff_draw)) {  
        
            throw std::invalid_argument("The previous value and coeff_draw must be the same shape.");
        }

        coeff_draw = default_value;
        
        std::cerr << "I am assigning the coeff_draw to the default value." << std::endl;

    } 
    
    return std::make_tuple(coeff_draw, innov_cov_draw);
}



/*
 * Returns a draw from a conditional normal distribution. It assumes that the vector you are drawing is 
 * contiguous. 
 */
template<typename RealType>
auto conditional_gaussian_moments(aw::npulong first_idx, const arma::Col<RealType>& realization, const
        arma::Col<RealType>& mean, const arma::Mat<RealType>& cov) {
    
    aw::claim_same_n_rows(mean, cov);
    aw::claim_same(cov.n_cols, cov.n_rows);
    assert(realization.n_rows < mean.n_rows);
    assert(first_idx < mean.n_rows);

    aw::assert_is_finite(realization, mean, cov);

    arma::Col<arma::uword> indices(mean.n_rows);
    std::iota(begin(indices), end(indices), 0);
    arma::Col<arma::uword> draw_indices(mean.n_rows - realization.n_rows);
    std::iota(begin(draw_indices), end(draw_indices), first_idx);
    arma::Col<arma::uword> realization_indices(realization.n_rows);
    std::set_difference(cbegin(indices), cend(indices), cbegin(draw_indices), cend(draw_indices), 
                        begin(realization_indices));     
    const arma::Mat<RealType> realization_root_cov = lt_root_mat(cov(realization_indices, realization_indices));   

    const arma::Mat<RealType> cov_draw_realization = cov(draw_indices, realization_indices);  

    const arma::Col<RealType> cond_mean = mean(draw_indices) + cov_draw_realization * 
        cho_solve(realization_root_cov, realization - mean(realization_indices));  
    
    const arma::Mat<RealType> cond_var = cov(draw_indices, draw_indices) - cov_draw_realization * 
         cho_solve(realization_root_cov, cov_draw_realization.t());  

    aw::assert_is_finite(cond_mean, cond_var);
    return std::make_tuple(cond_mean, cond_var); 

}


/*
 * Returns a draw from a conditional normal distribution. 
 */
template<typename RealType>
arma::Col<RealType> draw_conditional_gaussian(aw::npulong first_idx, const arma::Col<RealType>& realization, const
        arma::Col<RealType>& mean, const arma::Mat<RealType>& cov) { 


    arma::Mat<RealType> cond_cov;
    arma::Mat<RealType> cond_mean;


    std::tie(cond_mean, cond_cov) = conditional_gaussian_moments(first_idx, realization, mean, cov);

    const arma::Col<RealType> draw = cond_mean + arma::trimatl(lt_root_mat(cond_cov)) * randn(cond_cov.n_cols); 

    return draw; 

}



/*
 * Uses an acceptance-rejection algorithm to take draws from a truncated normal distribution. The number of 
 * samples is not deterministic, and hence the algorithm will likely perform rather poorly if the truncation 
 * binds a large majority of the time, but perform quite well if it only binds occasionally. At present we only 
 * allow for lower truncation, if you want upper truncation you need to exploit the symmetry of the normal 
 * distribution. We use the algorithm of Robert "Simulation of truncated normal vairables" (1995).
 */
template <typename RealType = double>
class truncated_normal_distribution { 

    private:
        
        const RealType l_trunc;
        const RealType mean;
        const RealType stddev;  


    public:

        inline explicit truncated_normal_distribution(RealType l_trunc=0, RealType mean=0, 
                                                      RealType stddev=1) : l_trunc{l_trunc}, 
                                                                            mean{mean}, stddev{stddev} {} 
        
        
        template<class Generator> 
        RealType operator()(Generator & g) { 
            RealType draw;

            
            if (l_trunc < mean) { 
               
               std::normal_distribution<RealType> n_dist(mean, stddev);   

                do { 
                    draw = n_dist(g); 
            
                } while (l_trunc > draw); 
                

            }  else {
                
                RealType cutoff_u;
                RealType cutoff;  

                do { 
                    RealType rescaled_trunc = (l_trunc - mean) / stddev;  
                    
                    RealType alpha_star = .5 * (rescaled_trunc + std::sqrt(std::pow(rescaled_trunc,2) + 4));
                    std::exponential_distribution<RealType> e_dist(alpha_star);
                    std::uniform_real_distribution<RealType> u_dist(0,1);

                    draw = rescaled_trunc + e_dist(g); 
     
                    cutoff = - .5 * std::pow(draw - alpha_star, 2);
                    cutoff_u = std::log(u_dist(g));  
                } while (cutoff_u > cutoff); 
                    
                draw = stddev * draw + mean;
            }

            return draw; 
        }

};



/* 
 * Computes the indices of each value in the arr, and returns a vector of uvecs of each value.
 * For example, it will turn { 1 2 2 1 3} -> {{}, {0, 3}, {1, 2}, {4}}.    
 */
template<typename ContainerType>
auto sort_indices(const ContainerType& arr) -> std::vector<std::vector<aw::npulong>> {

    aw::npulong max_val = (arr.size() > 0) ? *std::max_element(cbegin(arr), cend(arr)) : 0;
    /* I am creating max_val + 1 elements because I need to allow for zero. */ 
    std::vector<std::vector<aw::npulong>> indices_vec(max_val + 1, std::vector<aw::npulong>());

    for(aw::npulong idx=0; idx<arr.size(); ++idx) { 

        indices_vec[arr[idx]].push_back(idx);

    }
    
    return indices_vec; 

}




template<typename ValueType>
arma::Mat<ValueType> reshape_square(arma::Col<ValueType> vec) { 

    size_t side_length = static_cast<size_t>(std::sqrt(vec.size()));

    if (side_length * side_length != vec.size()) {
        throw std::invalid_argument("The array has "s 
                + std::to_string(vec.size()) + " number of elemnts, which is "s
                + "not a perfect square."s);
    } 

    arma::Mat<ValueType> return_mat = arma::conv_to<arma::Mat<ValueType>>::from(vec);
    
    return arma::reshape(return_mat, side_length, side_length); 

}
    
template<typename ValueType>
arma::Mat<ValueType> reshape_square(arma::Mat<ValueType> mat) { 

    size_t side_length = static_cast<size_t>(std::sqrt(mat.size()));

    if (side_length * side_length != mat.size()) {
        throw std::invalid_argument("The array has "s 
                + std::to_string(mat.size()) + " number of elemnts, which is "s
                + "not a perfect square."s);
    } 

    return arma::reshape(mat, side_length, side_length); 
}

template<typename ValueType>
arma::Mat<ValueType> reshape_square(std::vector<ValueType> vec) { 

    return reshape_square(arma::conv_to<arma::Col<ValueType>>::from(vec));

}


/* End namespace met */
} 

#endif /* METROP_H */
