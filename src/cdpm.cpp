#include "cdpm.h"
#include "metropolis.h"
#include <arma_wrapper.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "pybind11/operators.h"
#include <pybind11/iostream.h>
#include <random>
#include <stdexcept>
#include <unordered_map>

namespace py = pybind11;
using namespace pybind11::literals; 
using cdpm::npulong;
using cdpm::dmat;
using cdpm::dcube;
using cdpm::vec_map; 
using cdpm::vec_nested_map; 
using stream_redirect = py::call_guard<py::scoped_ostream_redirect>;



/* Provide a wrapper of the condtional gaussian moments function in the header filer. */
auto conditional_gaussian_moments(npulong first_idx, const cdpm::dvec& real, const cdpm::dvec& mean, const dmat&
        cov) {

    return  met::conditional_gaussian_moments(first_idx, real, mean, cov);
    
}


cdpm::cdpm make_cdpm1(const arma::Mat<aw::npint>& cluster_id, const dcube& beta, const dcube& comp_cov, const dmat&
        regressor, const dmat& regressand, const vec_nested_map& prior) { 

   cdpm::iuvec converted_cluster_id = arma::conv_to<cdpm::iuvec>::from(cluster_id);
   return cdpm::cdpm(converted_cluster_id, beta, comp_cov, regressor, regressand, prior);  

}

cdpm::cdpm make_cdpm2(const cdpm::iuvec& cluster_id, const dmat& regressor, const dmat& regressand, const
        vec_nested_map& prior) {

   cdpm::iuvec converted_cluster_id = arma::conv_to<cdpm::iuvec>::from(cluster_id);

   return cdpm::cdpm(converted_cluster_id, regressor, regressand, prior);  

}



auto operator_call(cdpm::cdpm * model, bool draw_parameters=true, bool draw_indices=true) {
    
    return (*model)(draw_parameters, draw_indices);

}


std::tuple<dmat, dmat> draw_component_specific_params(const cdpm::iuvec& cluster_id, const dmat& regressor, const
        dmat& regressand, const vec_nested_map& prior_arg) {
        
    cdpm::cdpm_prior prior(prior_arg, regressor.n_cols);  

    return cdpm::draw_component_specific_params(cluster_id, regressor, regressand, prior); 

}

std::tuple<dmat, dmat> draw_OLS_params(const dmat& regressor, const dmat& regressand, vec_nested_map& prior) {
        return cdpm::draw_OLS_params(regressor, regressand, cdpm::cdpm_prior(prior, regressor.n_cols)); 

}


PYBIND11_MODULE(libcdpm, m) { 

    m.def("_normal_log_pdf", 
         [] (cdpm::npdouble x, cdpm::npdouble mean, cdpm::npdouble stddev) { return met::normal_log_pdf(x, mean,
             stddev);}, stream_redirect(), "The log density of a gaussian random variable.", "x"_a, "mean"_a,
         "scale"_a); 

    m.def("_predict_ols", &cdpm::predict_ols, stream_redirect(), 
            "Creates the one-step ahead foreacast for a VAR(1).", "data"_a, "beta"_a, "cov"_a);

    /* 
     * Calling this object from python will copy all of the parameters in each direction. Because I am only 
     * exposing this function to python for testing purposes, this should not be a problem. 
     */
    m.def("_sort_indices", [] (const std::vector<npulong> vec) {return met::sort_indices(vec);},
            stream_redirect(),
             "Computes the indices each value in an array and retuns a vector of unsigned integers of each value. "
             "For example. it will turn { 1 2 2 1 3 } -> {{}, {0, 3}, {1, 2},{4}}.", "arr"_a); 

    m.def("_conditional_gaussian_moments", &conditional_gaussian_moments,  stream_redirect(),
            "This function computes conditional momenets of a gaussian random variable.",
            "first_idx"_a,"realization"_a, "mean"_a, "cov"_a); 

    m.def("draw_sticks", &cdpm::draw_sticks, stream_redirect(), 
            "This function takes an array  of labels and the prior precision and returns a draw from the " 
            " posterior of the stick-breaking representation", "cluster_id"_a, "beta"_a);
    
    m.def("draw_u", &cdpm::draw_u, stream_redirect(),
            "Draw the uniform random variables as described in Walker (2007)", "cluster_id"_a, "probabilities"_a, 
            "trans_mat"_a);

    m.def("draw_transition_matrix", &cdpm::draw_transition_matrix, stream_redirect(),
            "This function draws the transition matrix.", "cluster_id"_a, "c"_a);

    m.def("compute_required_probabilities", &cdpm::compute_required_probabilities, stream_redirect(),
            "Computes the enough probabilities that all of the components land in one of the given ones."
            "sticks"_a, "min_u"_a, "alpha"_a);

    m.def("compute_probabilities", &cdpm::compute_probabilities, stream_redirect(),
            "Takes the stick-breaking representation and computes the implied probabilities." "sticks"_a);
    
    m.def("predict_cdpm", &cdpm::predict_cdpm, stream_redirect(),
            "Computes the one step-ahead forecasts of the model." "cluster_id"_a, "trans_mat"_a, "beta"_a,
            "comp_cov"_a, "regressor"_a);

    m.def("is_stationary", &met::is_stationary, stream_redirect(),
            "Checks whether the matrix is stationary, if it is not square, it assumes that the bottom rows "
            "correspond to the VAR coefficients, i.e. the mean is place on top.", "matrix"_a);

    m.def("_draw_component_specific_params", &draw_component_specific_params, stream_redirect(),
            "Draws the component covariance matrix and beta coefficient component by component.",
            "cluster_id"_a, "regressor"_a, "regressand"_a, "prior"_a);

    m.def("draw_OLS_params", &draw_OLS_params, stream_redirect(),
            "Draws the component covariance matrix and beta coefficient for a OLS (Bayesian Regression)",
            "regressor"_a, "regressand"_a, "prior"_a);

    py::class_<cdpm::cdpm>(m, "CondDirichletProcessMix") 
        .def(py::init(&make_cdpm1), stream_redirect(), "cluster_id"_a, "beta"_a, "comp_cov"_a, "regressor"_a,
                "regressand"_a, "prior"_a) 
        .def(py::init(&make_cdpm2), stream_redirect(), "cluster_id"_a, "regressor"_a, "regressand"_a, "prior"_a) 
        .def(py::init<const dmat&, const dmat&, const dmat&, const dmat&, const vec_nested_map&>(), 
                stream_redirect(), "Constructs an object with only one component.", 
                "beta"_a, "comp_cov"_a, "regressor"_a, "regressand"_a, "prior"_a)
        .def("__call__",   &operator_call, stream_redirect(), 
                "Returns a draw from the posterior and updates the state.", "draw_parameters"_a=true,
                "draw_indices"_a=true)
        .def_property("regressor", &cdpm::cdpm::get_regressor,  [] (cdpm::cdpm& instance, const dmat& x)
                {instance.set_regressor(x);})
        .def_property("regressand", &cdpm::cdpm::get_regressand, [] (cdpm::cdpm& instance, const dmat& x)
                {instance.set_regressand(x);})
        .def_property("comp_cov", &cdpm::cdpm::get_comp_cov, [] (cdpm::cdpm& instance, const dcube& x)
                {instance.set_comp_cov() = x;})
        .def_property("cluster_id", &cdpm::cdpm::get_cluster_id, [] (cdpm::cdpm& instance, const cdpm::iuvec& x)
                {instance.set_cluster_id(x);})
        .def_property("trans_mat", &cdpm::cdpm::get_trans_mat,  [] (cdpm::cdpm& instance, const dmat& x)
                {instance.set_trans_mat() = x;})
        .def_property("coeffs", &cdpm::cdpm::get_beta, [] (cdpm::cdpm& instance, const dcube& x)
                {instance.set_beta() = x; })
        .def_property("sticks", &cdpm::cdpm::get_sticks, [] (cdpm::cdpm& instance, const cdpm::dvec& x)
                {instance.set_sticks() = x;})
        .def_property_readonly("time_dim", &cdpm::cdpm::time_dim)
        .def_property_readonly("cluster_dim", &cdpm::cdpm::cluster_dim)
        .def_property_readonly("regressor_dim", &cdpm::cdpm::regressor_dim)
        .def_property_readonly("regressand_dim", &cdpm::cdpm::regressand_dim)
        .def_property_readonly("probabilities", &cdpm::cdpm::get_probabilities);

    py::class_<cdpm::cov_prior_hierarchical>(m, "CovHierarchy") 
        .def(py::init<const vec_map&>(), stream_redirect(), "Constructs a prior object.")
        .def_property_readonly("dim", &cdpm::cov_prior_hierarchical::dim)
        .def_readonly("mu1", &cdpm::cov_prior_hierarchical::mu1)
        .def_readonly("mu2", &cdpm::cov_prior_hierarchical::mu2)
        .def_readonly("scale_params", &cdpm::cov_prior_hierarchical::scale_params)
        .def_property_readonly("scale",
                [] (const cdpm::cov_prior_hierarchical& instance) {return instance.scale();}) 
        .def_property_readonly("scales", &cdpm::cov_prior::scales)
        .def("fit", &cdpm::cov_prior_hierarchical::fit, stream_redirect(), "Updated the scale of the model.")
        .def("draw_hyperprior_scale",  &cdpm::cov_prior_hierarchical::draw_hyperprior_scale, stream_redirect(), 
                "Returns a prior scale of the scale parameter.")
        .def("draw", &cdpm::cov_prior_hierarchical::draw, stream_redirect(), 
                "Returns a draw from the prior.")
        // The argument to the c++ function cond_draw is ignored. 
        .def("cond_draw", [] (cdpm::cov_prior_hierarchical & instance) {return instance.cond_draw(0);}, 
                stream_redirect(), "Returns a draw from the conditional prior.")
        .def("cond_draw", [] (cdpm::cov_prior_hierarchical & instance, size_t h) {return instance.cond_draw(h); }, 
                stream_redirect(), "Returns a draw from the conditional prior.");

    py::class_<cdpm::cov_prior_F_dist>(m, "Cov2Hierarchy") 
        .def(py::init<const vec_map&>(), stream_redirect(), "Constructs a prior object.")
        .def_property_readonly("dim", &cdpm::cov_prior_F_dist::dim)
        .def_readonly("mu1", &cdpm::cov_prior_F_dist::mu1)
        .def_readonly("mu2", &cdpm::cov_prior_F_dist::mu2)
        .def_readonly("scale_params", &cdpm::cov_prior_F_dist::scale_params)
        .def_property_readonly("scales", &cdpm::cov_prior_F_dist::scales)
        .def("scale", &cdpm::cov_prior_F_dist::scale, stream_redirect(), "Get scales from the model.", "h"_a)
        .def("fit", &cdpm::cov_prior_F_dist::fit, stream_redirect(), "Updated the scale of the model.")
        .def("draw_hyperprior_scale",  &cdpm::cov_prior_F_dist::draw_hyperprior_scale, stream_redirect(), 
                "Returns a prior scale of the scale parameter.")
        .def("draw", &cdpm::cov_prior_F_dist::draw, stream_redirect(), 
                "Returns a draw from the prior.")
        .def("cond_draw", &cdpm::cov_prior_F_dist::cond_draw, stream_redirect(), 
                "Returns a draw from the conditional prior.");

    py::class_<cdpm::simple_coeff_prior>(m, "SimpleCoeffPrior") 
        .def(py::init<const vec_map&>(), stream_redirect(), "Constructs a prior object.")
        .def(py::init<>(), stream_redirect(), "Constructs a prior object.")
        .def_readonly("mean", &cdpm::simple_coeff_prior::mean)
        .def_readonly("covariance", &cdpm::simple_coeff_prior::covariance)
        .def("draw", &cdpm::simple_coeff_prior::draw, stream_redirect(), "Returns a draw from the prior."
                "stationary"_a=false, "num_stationary_tries"_a=100)
        .def("fit", &cdpm::simple_coeff_prior::fit, stream_redirect(), "Updates the hyperparamters");

    py::class_<cdpm::hierarchical_coeff_prior>(m, "HierarchicalCoeffPrior") 
        .def(py::init<const vec_map&>(), stream_redirect(), "Constructs a prior object.")
        .def(py::init<>(), stream_redirect(), "Constructs a prior object.")
        .def_readonly("mean", &cdpm::hierarchical_coeff_prior::mean)
        .def_readonly("hypermean", &cdpm::hierarchical_coeff_prior::hypermean)
        .def_readonly("covariance", &cdpm::hierarchical_coeff_prior::covariance)
        .def("draw", &cdpm::hierarchical_coeff_prior::draw, stream_redirect(), "Returns a draw from the prior.",
                "stationary"_a=false, "num_stationary_tries"_a=100)
        .def("fit", &cdpm::hierarchical_coeff_prior::fit, stream_redirect(), "Updates the hyperparamters");

}
